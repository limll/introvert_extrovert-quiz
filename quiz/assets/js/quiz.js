(function () {

  "use strict";

  var App = angular.module("App.controllers", ["ngSanitize"]);

  App.controller("MainCtrl", ["$scope", "$http", function ($scope, $http) {

    $scope.quiz = [];
    $scope.id = 0;
    $scope.percentage = 0;
    $scope.qns = "";
    $scope.score = 0;


    $scope.quiz_started = false;
    $scope.quiz_ended = false;
    $scope.showAns = false;
    $scope.shareFB_url = "http://dsproject.zaobao.com/0.demo/ready-made-quiz/assets/share.php?score=" + $scope.score +"&type=fb";

    $scope.loadData = function (successcb, failurecb) {
      $http({
        method: "GET",
        url:
          "assets/json/index.json?" +
          Date.now(),
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(
              encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])
            );
          return str.join("&");
        },
        data: {}
      })
        .success(function($data, $status, $headers, $config) {
          if ($data) {
            $scope.quiz = $data.questions;
            $scope.getQuestion();
          } else {
          }
        })
        .error(function($data, $status, $headers, $config) {
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
    }

    $scope.loadData();

    $scope.getQuestion = function(){
      $scope.qns = $scope.quiz[$scope.id]['qns'];
      $scope.qns_img = $scope.quiz[$scope.id]["img"];
      $scope.choices = $scope.quiz[$scope.id]["choices"];
      $scope.qns_num = $scope.quiz[$scope.id]["id"];

    }

    $scope.start = function(){
      $scope.quiz_started = true;
  
    }

    $scope.start();
    $scope.calculateScore = function(ans){

      if(ans == "correct"){
        $scope.score = $scope.score + 1;
        $scope.result = true;
      } else {
        $scope.result = false;
      }
      
    }

    
    $scope.click_choice = function(id, ans){
     
      $scope.wrong_ans = 0;
      $scope.right_ans = 0;
      $scope.show_tick = false;

      $scope.coin_animate = false;
      // $("#crypto-quiz > div:last-child").css('display', "none");
      // if (ans == "wrong") {
      //   console.log('a');
      //   $scope.wrong_ans = id;
      // } else if (ans == "correct") {
      //   console.log("b");
      //   $scope.right_ans = id;
      //   $scope.show_tick = true;
      // }
   
      if(ans == 'wrong'){
        $scope.wrong_ans = id;
        // $scope.show_tick = false;
        for (var i = 0; i < $scope.choices.length; i++) {
          if ($scope.choices[i]['ans'] == "correct") {
            $scope.right_ans = $scope.choices[i]['id'];
            
          }
        }
      } else {
        $scope.right_ans = id;
        $scope.show_tick = true;

        if ($scope.coin_animate == false){
          // $("#crypto-quiz > div:last-child").css('display', "block");
    
       
        };
      
      }
      
    }
    $scope.getDescription = function(index){
      $scope.showAns = true;
      $scope.chosen_choice = $scope.choices[index];
    
      // $scope.description = $scope.choices[index]['description'];
      $scope.para = $scope.quiz[$scope.id]["para"];
      $scope.references = $scope.quiz[$scope.id]["references"];
      
      for(var i = 0; i < $scope.choices.length; i++){
        if($scope.choices[i]['ans'] == "correct"){
          $scope.correct_ans = $scope.choices[i];
        }
      }

    }

    $scope.nextQns = function(){
      $scope.id += 1;
      $scope.showAns = false;

      $('html, body').animate({
        scrollTop: $("#quiz").offset().top
      }, 100);
    }

    $scope.prevQns = function () {
      $scope.id -= 1;
      $scope.showAns = true;
    }

   

    $scope.showResult = function(){
      $scope.quiz_ended = true;
      $scope.submitResult();

      switch ($scope.score) {
        case 0:
          $scope.coin_img = "assets/imgs/score-image-1.png";
          break;
        case 1:
          $scope.coin_img = "assets/imgs/score-image-1.png";
          break;
        case 2:
          $scope.coin_img = "assets/imgs/score-image-1.png";
          break;
        case 3:
          $scope.coin_img = "assets/imgs/score-image-2.png";
          break;
        case 4:
          $scope.coin_img = "assets/imgs/score-image-3.png";
          break;
        case 5:
          $scope.coin_img = "assets/imgs/score-image-3.png";
          break;
      }

      if($scope.score >= 0 && $scope.score <=2) {
        $scope.category = "";
        $scope.other_category = "";
        $scope.result_desc = "<p class='desc'>看来你和月亮的距离还很远，多吃点月饼吧！<p>";
        $scope.references = []
      } else if ($scope.score == 3) {
        $scope.category = "";
        $scope.other_category = "";
        $scope.result_desc = "<p class='desc'>表现不错哦，嫦娥是你的朋友？</p>";
        $scope.references = []
      } else {
        $scope.category = "";
        $scope.other_category = "";
        $scope.result_desc = "<p class='desc'>厉害！下一次登月靠你了！</p>";
        $scope.references = []
      }
      
      $('html, body').animate({
        scrollTop: $("#quiz").offset().top
      }, 100);
    
     
    }

    $scope.submitResult = function (successcb, failurecb){
      
      $http({
        method: "POST",
        url: "assets/php/submit.php",
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Cache-Control": "no-cache"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(
              encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])
            );
          return str.join("&");
        },
        data: {
          firstname: "X",
          lastname: "X",
          email: "ready-made-quiz@gmail.com"
        }
      })
        .success(function($data, $status, $headers, $config) {
          if ($data) {
            // console.log($data);
          } else {
          }
        })
        .error(function($data, $status, $headers, $config) {
          // console.log($data);
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
    }

   

    $scope.restart = function(){
      $scope.quiz = [];
      $scope.id = 0;
      $scope.percentage = 0;
      $scope.qns = "";
      $scope.score = 0;
      $scope.showAns = false;

      $scope.quiz_started = true;
      $scope.quiz_ended = false;
      $scope.loadData();
    }

   
   

    
  }])

}())
