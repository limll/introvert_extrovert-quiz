<?php
if(!empty($_GET))
{
  $http_host = "http://" . $_SERVER['HTTP_HOST'];
  $url_components = parse_url($http_host . str_replace(basename(__FILE__), "", $_SERVER['REQUEST_URI']));

  $proj_base_url = $http_host;

  $share_type = (isset($_GET['type'])) ? $_GET['type'] : NULL;

  $score = $_GET['score'];
  $percentage = $_GET['percentage'];

  
  $entries_data = [
    '0' => [
      'title' => "我在加密货币测试中答对了0题， 我是“加密货币菜鸟”！",
      'image' => 'fb-share-0.png?v1',
      'description' => ''
    ],
    '1' => [
      'title' => "我在加密货币测试中答对了1题， 我是“加密货币菜鸟”！",
      'image' => 'fb-share-1.png?v1',
      'description' => ''
    ],
    '2' => [
      'title' => '我在加密货币测试中答对了2题， 我是“加密货币菜鸟”！',
      'image' => 'fb-share-2.png?v1',
      'description' => ''
    ],
    '3' => [
      'title' => '我在加密货币测试中答对了3题， 我是“加密货币菜鸟”！',
      'image' => 'fb-share-3.png?v1',
      'description' => ''
    ],
    '4' => [
      'title' => '我在加密货币测试中答对了4题， 我是“加密货币菜鸟”！',
      'image' => 'fb-share-4.png?v1',
      'description' => ''
    ],
    '5' => [
      'title' => '我在加密货币测试中答对了5题， 我是“加密货币菜鸟”！',
      'image' => 'fb-share-5.png?v1',
      'description' => ''
    ],
    '6' => [
      'title' => '我在加密货币测试中答对了6题， 我是“加密货币普通咖”！',
      'image' => 'fb-share-6.png?v1',
      'description' => ''
    ],
    '7' => [
      'title' => '我在加密货币测试中答对了7题， 我是“加密货币普通咖”！',
      'image' => 'fb-share-7.png?v1',
      'description' => ''
    ],
    '8' => [
      'title' => '我在加密货币测试中答对了8题， 我是“加密货币普通咖”！',
      'image' => 'fb-share-8.png?v1',
      'description' => ''
    ],
    '9' => [
      'title' => '我在加密货币测试中答对了9题， 我是“加密货币达人”！',
      'image' => 'fb-share-9.png?v1',
      'description' => ''
    ],
    '10' => [
      'title' => '我在加密货币测试中答对了10题， 我是“加密货币达人”！',
      'image' => 'fb-share-10.png?v1',
      'description' => ''
    ]
  ];

  //------------------------------------------------------------------------

  $entry = strtoupper($_GET['score']);


  if(array_key_exists($entry, $entries_data))
  {
    $data = $entries_data[$entry];

    $shareUrl = $proj_base_url . "/2019/cryptocurrency-quiz/assets/php/share.php?score=" . $entry;
    // $redirection_url = $proj_base_url . "/2017/voting-2017.php#Modal" . $entry;
    $redirection_url = " //www.zaobao.com/interactive-graphics/story20190104-920845"; 
    $shareImg = $proj_base_url . "/2019/cryptocurrency-quiz/assets/imgs/" . $data['image'];
    ?>
      <html lang="en" itemscope itemtype="http://schema.org/Blog">
      <title><?php print $data['title']; ?></title>
      <head>
          <meta charset="UTF-8">

          <meta property="og:locale" content="en_GB"/>
        <!--   <meta property="fb:app_id" content="184683071273"/> -->
          <meta property="og:title" content="<?php print $data['title'] ?>"/>
          <meta property="og:description"
                content="你比我厉害吗？"/>
          <meta property="og:site_name" content='联合早报'/>
          <meta property="og:type" content="website"/>
          <meta property="og:image" content="<?php print $shareImg; ?>"/>
          <meta property="og:image:width" content="1200"/>
          <meta property="og:image:height" content="630"/>
          <meta property="og:url" content="<?php print $shareUrl; ?>"/>
           <meta http-equiv="refresh"
                  content="1; url=<?php print $redirection_url; ?>"/>

        <!-- <?php if ($share_type == 'fb'): ?> -->
           <!--  <meta http-equiv="refresh"
                  content="1; url=<?php print $redirection_url; ?>"/> -->
     <!--    <?php endif; ?> -->
      </head>
      <body>
      <!-- <p style='text-align: center; font-family: Arial; margin-top: 25%; color: #555;'><?php print $data['title']; ?></p> -->
       <script type="text/javascript">
              window.location.replace("<?php print $redirection_url; ?>");
        </script>
        <?php if ($share_type == 'fb'): ?>
            <script type="text/javascript">
                window.location.replace("<?php print $redirection_url; ?>");
            </script>
        <?php endif; ?> 
      </body>
      </html>
    <?php
  }
  else
  {
    include $_SERVER['DOCUMENT_ROOT'] . "/index.html";
  }
}
else
{
    include $_SERVER['DOCUMENT_ROOT'] . "/index.html";
}
?>