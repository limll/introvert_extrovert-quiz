<?php
//TAKE NOTE WHEN IN SGADMIN IT WON'T WORK. WILL ONLY WORK AFTER IT HAS BEEN PUBLISHED
// Allow from any origin
if (isset($_SERVER['HTTP_ORIGIN'])) {
    // should do a check here to match $_SERVER['HTTP_ORIGIN'] to a
    // whitelist of safe domains
    header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
    header('Access-Control-Allow-Credentials: true');
    header('Access-Control-Max-Age: 86400');    // cache for 1 day
}
// Access-Control headers are received during OPTIONS requests
if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD']))
        header("Access-Control-Allow-Methods: GET, POST, PUT, DELETE, OPTIONS");         

    if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']))
        header("Access-Control-Allow-Headers: {$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");

}

$data = $_POST;

//Development
$data['event_id'] = '86525280';
// Production
// $data['event_id'] = '92094234';

// $data['tickets']["34581"] = 1; //IF only one ticket, this value is not required
$ch = curl_init();


curl_setopt($ch, CURLOPT_URL,            "https://reg-middleware.gevme.com/reg-prod/create" );
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt($ch, CURLOPT_POST,           1 );
curl_setopt($ch, CURLOPT_POSTFIELDS,     json_encode($data));
curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: application/json','X-API-Key:2PRZppm3H17enDkP3oEwW7zzGScmVK4l2sd9vino'));
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

var_dump(curl_exec($ch));

if (curl_error($ch)) {
    $error_msg = curl_error($ch);
}
curl_close($ch);


if (isset($error_msg)) {
  echo "error";
  echo json_encode($error_msg);

}

?>