<?php
if(!empty($_GET))
{
  $http_host = "http://" . $_SERVER['HTTP_HOST'];
  $url_components = parse_url($http_host . str_replace(basename(__FILE__), "", $_SERVER['REQUEST_URI']));

  $proj_base_url = $http_host;

  $share_type = (isset($_GET['type'])) ? $_GET['type'] : NULL;

  $score = $_GET['score'];
  $percentage = $_GET['percentage'];

  
  $entries_data = [
    '-1' => [
      'title' => "我测出疫情期间的自己是“10% 内向”！",
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '-2' => [
      'title' => '我测出疫情期间的自己是“20% 内向”！',
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '-3' => [
      'title' => '我测出疫情期间的自己是“30% 内向”！',
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '-4' => [
      'title' => '我测出疫情期间的自己是“40% 内向”！',
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '-5' => [
      'title' => '我测出疫情期间的自己是“50% 内向”！',
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '-6' => [
      'title' => '我测出疫情期间的自己是“60% 内向”！',
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '-7' => [
      'title' => '我测出疫情期间的自己是“70% 内向”！',
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '-8' => [
      'title' => '我测出疫情期间的自己是“80% 内向”！',
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '-9' => [
      'title' => '我测出疫情期间的自己是“90% 内向”！',
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '-10' => [
      'title' => '我测出疫情期间的自己是“100% 内向”！',
      'image' => 'introvert-fb-sharing.png',
      'description' => ''
    ],
    '0' => [
      'title' => "我测出疫情期间的自己是“中间性格”",
      'image' => 'ambivert-fb-sharing.png',
      'description' => ''
    ],
    '1' => [
      'title' => "我测出疫情期间的自己是“10% 外向”！",
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ],
    '2' => [
      'title' => '我测出疫情期间的自己是“20% 外向”！',
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ],
    '3' => [
      'title' => '我测出疫情期间的自己是“30% 外向”！',
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ],
    '4' => [
      'title' => '我测出疫情期间的自己是“40% 外向”！',
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ],
    '5' => [
      'title' => '我测出疫情期间的自己是“50% 外向”！',
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ],
    '6' => [
      'title' => '我测出疫情期间的自己是“60% 外向”！',
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ],
    '7' => [
      'title' => '我测出疫情期间的自己是“70% 外向”！',
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ],
    '8' => [
      'title' => '我测出疫情期间的自己是“80% 外向”！',
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ],
    '9' => [
      'title' => '我测出疫情期间的自己是“90% 外向”！',
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ],
    '10' => [
      'title' => '我测出疫情期间的自己是“100% 外向”！',
      'image' => 'extrovert-fb-sharing.png',
      'description' => ''
    ]
  ];

  //------------------------------------------------------------------------

  $entry = strtoupper($_GET['score']);


  if(array_key_exists($entry, $entries_data))
  {
    $data = $entries_data[$entry];

    $shareUrl = $proj_base_url . "/2020/introvert-extrovert-quiz/assets/php/share.php?score=" . $entry;
    // $redirection_url = $proj_base_url . "/2017/voting-2017.php#Modal" . $entry;
    $redirection_url = "//www.zaobao.com.sg/interactive-graphics/story20200811-1072619"; 
    $shareImg = $proj_base_url . "/2020/introvert-extrovert-quiz/assets/imgs/" . $data['image'];
    ?>
      <html lang="en" itemscope itemtype="http://schema.org/Blog">
      <title><?php print $data['title']; ?></title>
      <head>
          <meta charset="UTF-8">

          <meta property="og:locale" content="en_GB"/>
        <!--   <meta property="fb:app_id" content="184683071273"/> -->
          <meta property="og:title" content="<?php print $data['title'] ?>"/>
          <meta property="og:description"
                content="你也做测试了解自己的性格吧！"/>
          <meta property="og:site_name" content='联合早报'/>
          <meta property="og:type" content="website"/>
          <meta property="og:image" content="<?php print $shareImg; ?>"/>
          <meta property="og:image:width" content="1200"/>
          <meta property="og:image:height" content="630"/>
          <meta property="og:url" content="<?php print $shareUrl; ?>"/>
           <meta http-equiv="refresh"
                  content="1; url=<?php print $redirection_url; ?>"/>

        <!-- <?php if ($share_type == 'fb'): ?> -->
           <!--  <meta http-equiv="refresh"
                  content="1; url=<?php print $redirection_url; ?>"/> -->
     <!--    <?php endif; ?> -->
      </head>
      <body>
      <!-- <p style='text-align: center; font-family: Arial; margin-top: 25%; color: #555;'><?php print $data['title']; ?></p> -->
       <script type="text/javascript">
              window.location.replace("<?php print $redirection_url; ?>");
        </script>
        <?php if ($share_type == 'fb'): ?>
            <script type="text/javascript">
                window.location.replace("<?php print $redirection_url; ?>");
            </script>
        <?php endif; ?> 
      </body>
      </html>
    <?php
  }
  else
  {
    include $_SERVER['DOCUMENT_ROOT'] . "/index.html";
  }
}
else
{
    include $_SERVER['DOCUMENT_ROOT'] . "/index.html";
}
?>