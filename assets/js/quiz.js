(function () {

  "use strict";

  var App = angular.module("App.controllers", ["ngSanitize"]);

  App.controller("MainCtrl", ["$scope", "$http", function ($scope, $http) {

    $scope.quiz = [];
    $scope.id = 0;
    $scope.img = 0;
    $scope.percentage = 0;
    $scope.qns = "";
    $scope.score = 0;
    $scope.status = "";


    $scope.quiz_started = false;
    $scope.quiz_ended = false;
    $scope.showAns = false;
    $scope.shareFB_url = "";

    var initial_progress = 0;
    function setProgress(num){
      
        var $spinner = $('.progress-circle .spinner');
        var $filler = $('.progress-circle .filler');
        var $percentage = $('.progress-circle .percentage');
      
        if (num < 0) num = 0;
        if (num > 100) num = 100;
        
        var initialNum = $percentage.text().replace('%','');
        $({numVal:initialNum}).animate({numVal:num},{
          duration: 1000,
          easing: 'swing',
          step: function (currVal) {
          
            $percentage.text(Math.ceil(currVal) + '%');
            if (currVal > 0 && currVal <= 50) {
              $filler.css('display', 'none');
              var spinnerDegs = -180 + ((currVal * 180) / 50);
              rotate($spinner, spinnerDegs);
            } else if (currVal > 50) {
              rotate($spinner, 0);
              $filler.css('display', '');
              var fillerDegs = 0 + ((currVal * 180) / 50);
              rotate($filler, fillerDegs);
            }
          }
        });
      
      }
      
      function rotate($el, deg) {
        $el.css({
          '-webkit-transform': 'rotate(' + deg + 'deg)',
          '-moz-transform': 'rotate(' + deg + 'deg)',
          '-ms-transform': 'rotate(' + deg + 'deg)',
          '-o-transform': 'rotate(' + deg + 'deg)',
          'transform': 'rotate(' + deg + 'deg)'
        });
      }
    
      setProgress(0);




    $scope.loadData = function (successcb, failurecb) {
      $scope.quiz = [{
        "id": 1,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn1-bg.svg",
        "mobile_img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn1-mobile-bg.svg",
        "qns": "星期五的晚上，你喜欢",
        "choices": [{
            "id": "A",
            "choice": "出去玩",
            "value": 1
            
        }, {
            "id": "B",
            "choice": "回家",
            "value": -1
        }]
    },{
        "id": 2,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn2-bg.svg",
        "mobile_img":"//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn2-mobile-bg.svg",
        "qns": "在派对上，你会站在哪里？",
        "choices": [{
            "id": "A",
            "choice": "人群中",
            "value": 1
        }, {
            "id": "B",
            "choice": "角落",
            "value": -1
        }]
    },{
        "id": 3,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn3-bg.svg",
        "mobile_img":"//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn3-mobile-bg.svg",
        "qns": "你更喜欢哪种互动？",
        "choices": [{
            "id": "A",
            "choice": "大群人",
            "value": 1
        }, {
            "id": "B",
            "choice": "小组",
            "value": -1
        }]
    },{
        "id": 4,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn4-bg.svg",
        "mobile_img":"//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn4-mobile-bg.svg",
        "qns": "你喜欢焦点放在自己身上吗？",
        "choices": [{
            "id": "A",
            "choice": "不喜欢",
            "value": -1
        }, {
            "id": "B",
            "choice": "喜欢",
            "value":1
           
        }]
    },{
        "id": 5,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn5-bg.svg",
        "mobile_img":"//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn5-mobile-bg.svg",
        "qns": "一个理想的约会是在",
        "choices": [{
            "id": "A",
            "choice": "树林里",
            "value": -1
        }, {
            "id": "B",
            "choice": "城市里",
            "value": 1
        }]
    },{
        "id": 6,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn6-bg.svg",
        "mobile_img":"//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn6-mobile-bg.svg",
        "qns": "朋友临时邀你出去玩，你会",
        "choices": [{
            "id": "A",
            "choice": "答应",
            "value": 1
        }, {
            "id": "B",
            "choice": "回绝",
            "value": -1
        }]
    },{
        "id": 7,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn7-bg.svg",
        "mobile_img":"//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn7-mobile-bg.svg",
        "qns": "朋友更可能形容你为",
        "choices": [{
            "id": "A",
            "choice": "内敛",
            "value": -1
        }, {
            "id": "B",
            "choice": "外放",
            "value": 1
        }]
    },{
        "id": 8,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn8-bg.svg",
        "mobile_img":"//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn8-mobile-bg.svg",
        "qns": "和别人分享自己的心事，你需要",
        "choices": [{
            "id": "A",
            "choice": "很长时间",
            "value": -1
        }, {
            "id": "B",
            "choice": "不用多久",
            "value": 1
        }]
    },{
        "id": 9,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn9-bg.svg",
        "mobile_img":"//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn9-mobile-bg.svg",
        "qns": "结束一场社交活动后，你一般感到",
        "choices": [{
            "id": "A",
            "choice": "精力充沛",
            "value": 1
        }, {
            "id": "B",
            "choice": "精疲力尽",
            "value": -1
        }]
    },{
        "id": 10,
        "img": "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn10-bg.svg",
        "mobile_img":"//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/qn10-mobile-bg.svg",
        "qns": "个人空间对你来说",
        "choices": [{
            "id": "A",
            "choice": "重要",
            "value": -1
        }, {
            "id": "B",
            "choice": "不重要",
            "value": 1
        }]
    }]
    

    // NOT WORKING IN SGADMIN.ZAOBAO.COM BUT WORKING ON QA2.ZAOBAO.COM.SG. NOT SURE IF IT WILL WORK WHEN ARTICLE IS PUBLISHED. SO THE QNS ARE CODED ABOVE TO BE SAFE
      // $http({
      //   method: "GET",
      //   // url:
      //   //   "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/json/qns.json?" +
      //   //   Date.now(),
      //   url:
      //     "//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/php/getQns.php?" +
      //     Date.now(),
      //   async: true,
      //   cache: false,
      //   headers: {
      //     "Content-Type": "application/x-www-form-urlencoded"
      //   },
      //   transformRequest: function(obj) {
      //     var str = [];
      //     for (var p in obj)
      //       str.push(
      //         encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])
      //       );
      //     return str.join("&");
      //   },
      //   data: {}
      // })
      //   .success(function($data, $status, $headers, $config) {
      //     if ($data) {
      //       $scope.quiz = $data.questions;
      //       $scope.getQuestion();
      //     } else {
      //     }
      //   })
      //   .error(function($data, $status, $headers, $config) {
      //     // if (failurecb) {
      //     //     failurecb($data, $status, $headers, $config, "Error retreiving data");
      //     // }
      //   });
    }

    $scope.loadData();
   

    $scope.getQuestion = function(){
    
      $scope.qns = $scope.quiz[$scope.id]['qns'];
      $scope.qns_img = $scope.quiz[$scope.id]["img"];
      $scope.qns_mobile_img = $scope.quiz[$scope.id]["mobile_img"];
      $scope.choices = $scope.quiz[$scope.id]["choices"];
      $scope.qns_num = $scope.quiz[$scope.id]["id"];

    }

    $scope.getQuestion();

    $scope.start = function(){
      $scope.quiz_started = true;
  
    }

   
    $scope.calculateScore = function(ans){
      $scope.percentage = 0;
    //  console.log(ans)
      $scope.quiz.forEach(qns => {
        // console.log(qns);
        if((qns.id == ($scope.id+1))){
          let choices = qns.choices;
          choices.forEach(choice => {
            // console.log(choice)
            if(ans === choice.id){
              // console.log('asdasd');
              $scope.score += choice.value;
              
              $scope.percentage = Math.abs(($scope.score/10)*100);

              if($scope.score == 0){
                $scope.percentage = 100;
              }
            }
          })
          
        }
       
      });
      if($scope.score < 0){
        $scope.status = "内向"
      } else if($scope.score > 0){
        $scope.status = "外向"
      } else if($scope.score === 0){
        $scope.status = "中间性格"
      }

      if($scope.id === 9){
        $scope.submitResult();
      }
      
      
    }


  
    $scope.nextQns = function(ans){
      initial_progress += 1;
    // setProgress(Math.ceil(Math.random() * 100));
      setProgress(initial_progress * 10);
      if($scope.id !== 9){
        $scope.calculateScore(ans);
        $scope.id += 1;
        $scope.getQuestion();
      } else {
        $scope.calculateScore(ans);
        $scope.quiz_ended = true;
        $scope.showResult();
      }
      
      $('html, body').animate({
        scrollTop: $("#quiz").offset().top - 200
      }, 100);
    }

  

   

    $scope.showResult = function(){
      $scope.quiz_ended = true;
      // $scope.submitResult();
      $scope.shareFB_url = "http://interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/php/share.php?score=" + $scope.score +"&type=fb";

      if($scope.score < 0) {
       
        $scope.mood = "introvert";
        $scope.result_desc = "<p class='desc'>你重视个人空间，独处是你充电的方式。不过，内向的你需要社交吗？<p>";
        $scope.result_img = "<img src='//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/introvert-bg.svg'>";
        $scope.result_img_mobile = "<img src='//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/introvert-bg_mobile.svg'>";
       
      } else if ($scope.score == 0) {
      
        $scope.mood = "ambivert";
        $scope.result_desc = "<p class='desc'>既重视个人空间，又喜欢参加人多的聚会。兼具内向和外向两种特征的你，需要社交吗？社交时会感到焦虑吗？</p>";
        $scope.result_img = "<img src='//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/ambivert-bg.svg'>";
        $scope.result_img_mobile = "<img src='//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/ambivert-bg_mobile.svg'>";
       
      } else {
        
        $scope.mood = "extrovert";
        $scope.result_desc = "<p class='desc'>你喜欢交新朋友，喜欢参加人多的聚会。不过，外向的人社交时，会否感到焦虑？</p>";
        $scope.result_img = "<img src='//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/extrovert-bg.svg'>";
        $scope.result_img_mobile = "<img src='//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/extrovert-bg_mobile.svg'>";
        
      }
      
      $('html, body').animate({
        scrollTop: $("#quiz").offset().top
      }, 100);
    
     
    }

    $scope.submitResult = function (){
      
      $http({
        method: "POST",
        url: "//dsproject.zaobao.com/2020/introvert-extrovert-quiz/assets/php/submit.php?" + Date.now(),
        async: true,
        cache: false,
        headers: {
          "Content-Type": "application/x-www-form-urlencoded",
          "Cache-Control": "no-cache"
        },
        transformRequest: function(obj) {
          var str = [];
          for (var p in obj)
            str.push(
              encodeURIComponent(p) + "=" + encodeURIComponent(obj[p])
            );
          return str.join("&");
        },
        data: {
          firstname: "X",
          lastname: "X",
          email: "quiz@gmail.com",
          Fjffy1FA: $scope.score,
          cWU5zbLI: $scope.status,
          CeEAYPdQ: $scope.percentage + "%"
        }
      })
        .success(function($data, $status, $headers, $config) {
          if ($data) {
            // console.log($data);
          } else {
          }
        })
        .error(function($data, $status, $headers, $config) {
          // console.log($data);
          // if (failurecb) {
          //     failurecb($data, $status, $headers, $config, "Error retreiving data");
          // }
        });
    }

   

    $scope.restart = function(){
      $scope.quiz = [];
      $scope.id = 0;
      $scope.percentage = 0;
      $scope.qns = "";
      $scope.score = 0;
      $scope.showAns = false;

      $scope.quiz_started = true;
      $scope.quiz_ended = false;
      initial_progress = 0;
      $scope.loadData();
    }

   
   

    
  }])

}())
