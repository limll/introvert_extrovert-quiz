<body
  class="html not-front not-logged-in no-sidebars page-node page-node- page-node-1016181 node-type-article page page-article section-0-znews section-1-singapore is-ready is-loaded is-scrolled-up"
  style="">
  <div class="optanon-alert-box-wrapper  hide-cookie-setting-button" role="alertdialog"
    aria-labelledby="alert-box-title" aria-describedby="alert-box-message" style="display:none">
    <div class="optanon-alert-box-bottom-top">
      <div class="optanon-alert-box-corner-close">
        <button class="optanon-alert-box-close banner-close-button" aria-label="关闭" title="关闭"
          onclick="Optanon.TriggerGoogleAnalyticsEvent(&#39;OneTrust Cookie Consent&#39;, &#39;Banner Close Button&#39;);"
          tabindex="2"></button>
      </div>
    </div>
    <div class="optanon-alert-box-bg">
      <div class="optanon-alert-box-logo"> </div>
      <div class="optanon-alert-box-body">
        <p class="optanon-alert-box-title legacy-banner-title sr-only" id="alert-box-title" role="heading"
          aria-level="2">Cookie Notice</p>
        <p class="banner-content" id="alert-box-message">
          我们使用cookie来了解您如何使用我们的网站，从而优化您的阅读体验，如果您继续使用我们的网站，即表示您接受我们使用cookies。如您想了解更多，请点击<a
            href="https://sph.com.sg/legal/cookie-policy/" class="banner-policy-link" aria-label="这里。">这里。</a></p>
      </div>
      <div class="optanon-clearfix"></div>
      <div class="optanon-alert-box-button-container">
        <div class="optanon-alert-box-button optanon-button-close">
          <div class="optanon-alert-box-button-middle">
            <button class="optanon-alert-box-close" aria-label="关闭">关闭</button>
          </div>
        </div>
        <div class="optanon-alert-box-button optanon-button-allow">
          <div class="optanon-alert-box-button-middle accept-cookie-container">
            <button class="optanon-allow-all accept-cookies-button" title="接受" aria-label="接受"
              onclick="Optanon.TriggerGoogleAnalyticsEvent(&#39;OneTrust Cookie Consent&#39;, &#39;Banner Accept Cookies&#39;);"
              tabindex="1">接受</button>
          </div>
        </div>
      </div>
      <div class="optanon-clearfix optanon-alert-box-bottom-padding"></div>
    </div>
  </div>
  <div id="optanon" class="modern">
    <div id="optanon-popup-bg"></div>
    <div id="optanon-popup-wrapper" role="dialog" aria-modal="true" tabindex="-1" lang="zh-Hans">
      <div id="optanon-popup-top"></div>
      <div id="optanon-popup-body">
        <div id="optanon-popup-body-left">
          <div id="optanon-popup-body-left-shading"></div>
          <div id="optanon-branding-top-logo"
            style="background-image: url(&#39;https://cdn-apac.onetrust.com/skins/5.7.0/default_flat_bottom_two_button_black/v2/images/cookie-collective-top-logo.svg&#39;) !important;">
          </div>
          <ul id="optanon-menu" aria-label="Navigation Menu" role="tablist">
            <li class="menu-item-on menu-item-about" title="您的隐私"
              data-group="{&quot;GroupLanguagePropertiesSets&quot;:[{&quot;GroupName&quot;:{&quot;Text&quot;:&quot;您的隐私&quot;},&quot;GroupDescription&quot;:{&quot;Text&quot;:&quot;您访问任何网站时，网站都可能在您的浏览器上存储或检索信息，大多数是以 Cookie 的形式进行。此信息可能与您、您的偏好、您的设备相关，或者该信息被用于使网站按照您期望的方式工作。这些信息通常不会直接识别您，但它可为您提供更多个性化的 Web 体验。您可以选择不允许使用某些类型的 Cookie。单击不同类别标题以了解更多信息并更改默认设置。但是，您应该知道，阻止某些类型的 Cookie 可能会影响您的网站体验和我们能够提供的服务。&quot;}}]}">
              <p class="preference-menu-item">
                <button role="tab" aria-selected="true" aria-controls="您的隐私" id="您的隐私-undefined">您的隐私</button>
              </p>
            </li>
            <li class="menu-item-necessary menu-item-on" title="绝对必要的 Cookie"
              data-group="{&quot;ShowInPopup&quot;:true,&quot;Order&quot;:0,&quot;OptanonGroupId&quot;:1,&quot;Parent&quot;:null,&quot;GroupLanguagePropertiesSets&quot;:[{&quot;DefaultStatus&quot;:{&quot;Text&quot;:&quot;Always Active&quot;},&quot;GroupDescription&quot;:{&quot;Text&quot;:&quot;网站运行离不开这些 Cookie 且您不能在系统中将其关闭。通常仅根据您所做出的操作（即服务请求）来设置这些 Cookie，如设置隐私偏好、登录或填充表格。您可以将您的浏览器设置为阻止或向您提示这些 Cookie，但可能会导致某些网站功能无法工作。&quot;},&quot;GroupName&quot;:{&quot;Text&quot;:&quot;绝对必要的 Cookie&quot;},&quot;IsDntEnabled&quot;:false}],&quot;Cookies&quot;:[{&quot;Name&quot;:&quot;OptanonConsent&quot;,&quot;Host&quot;:&quot;zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:365},{&quot;Name&quot;:&quot;OptanonAlertBoxClosed&quot;,&quot;Host&quot;:&quot;zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:365},{&quot;Name&quot;:&quot;_dc_gtm_UA-39781780-2&quot;,&quot;Host&quot;:&quot;.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;NSC_JOsyyds4dpy0piyds5gqzldh3x1k5dt&quot;,&quot;Host&quot;:&quot;www.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:0}],&quot;Purposes&quot;:[],&quot;CustomGroupId&quot;:null,&quot;GroupId&quot;:1774}"
              data-optanongroupid="1">
              <p class="preference-menu-item">
                <button role="tab" aria-selected="false" aria-controls="绝对必要的-Cookie" id="绝对必要的-Cookie">绝对必要的
                  Cookie</button>
              </p>
            </li>
            <li class="menu-item-on menu-item-performance" title="性能 Cookie"
              data-group="{&quot;ShowInPopup&quot;:true,&quot;Order&quot;:1,&quot;OptanonGroupId&quot;:2,&quot;Parent&quot;:null,&quot;GroupLanguagePropertiesSets&quot;:[{&quot;DefaultStatus&quot;:{&quot;Text&quot;:&quot;Active&quot;},&quot;GroupDescription&quot;:{&quot;Text&quot;:&quot;使用 Cookie，我们可以计算访问量和流量来源，以便衡量和提高我们网站的性能。Cookie 有助于我们了解哪些页面最受欢迎、哪些最不受欢迎，并查看访问者如何浏览网站。这些 Cookie 收集的所有信息都聚合在一起，因此是匿名处理方式。如果您不允许使用这些 Cookie，我们将不知道您何时访问了我们的网站。&quot;},&quot;GroupName&quot;:{&quot;Text&quot;:&quot;性能 Cookie&quot;},&quot;IsDntEnabled&quot;:false}],&quot;Cookies&quot;:[{&quot;Name&quot;:&quot;_chartbeat2&quot;,&quot;Host&quot;:&quot;www.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:394},{&quot;Name&quot;:&quot;_gat_UA-39781780-2&quot;,&quot;Host&quot;:&quot;.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;_gid&quot;,&quot;Host&quot;:&quot;.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;atidvisitor&quot;,&quot;Host&quot;:&quot;.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:181},{&quot;Name&quot;:&quot;_ga&quot;,&quot;Host&quot;:&quot;.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:729},{&quot;Name&quot;:&quot;_gat_UA-124053137-2&quot;,&quot;Host&quot;:&quot;.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;AMP_TOKEN&quot;,&quot;Host&quot;:&quot;.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;_chartbeat4&quot;,&quot;Host&quot;:&quot;www.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;_gat_UA-124053137-3&quot;,&quot;Host&quot;:&quot;.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;_cb_ls&quot;,&quot;Host&quot;:&quot;www.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:394}],&quot;Purposes&quot;:[],&quot;CustomGroupId&quot;:null,&quot;GroupId&quot;:1770}"
              data-optanongroupid="2">
              <p class="preference-menu-item">
                <button role="tab" aria-selected="false" aria-controls="性能-Cookie" id="性能-Cookie">性能 Cookie</button>
              </p>
            </li>
            <li class="menu-item-on menu-item-functional" title="功能 Cookie"
              data-group="{&quot;ShowInPopup&quot;:true,&quot;Order&quot;:2,&quot;OptanonGroupId&quot;:3,&quot;Parent&quot;:null,&quot;GroupLanguagePropertiesSets&quot;:[{&quot;DefaultStatus&quot;:{&quot;Text&quot;:&quot;Active&quot;},&quot;GroupDescription&quot;:{&quot;Text&quot;:&quot;这些 Cookie 允许提供增强功能和个性化内容，如视频和实时聊天。我们或我们已将其服务添加至我们页面上的第三方提供者可以进行设置。如果您不允许使用这些 Cookie，则可能无法实现部分或全部功能的正常工作。&quot;},&quot;GroupName&quot;:{&quot;Text&quot;:&quot;功能 Cookie&quot;},&quot;IsDntEnabled&quot;:false}],&quot;Cookies&quot;:[{&quot;Name&quot;:&quot;has_js&quot;,&quot;Host&quot;:&quot;zproperty.zaobao.com.sg&quot;,&quot;IsSession&quot;:true,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;_cb_svref&quot;,&quot;Host&quot;:&quot;www.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:0}],&quot;Purposes&quot;:[],&quot;CustomGroupId&quot;:null,&quot;GroupId&quot;:1771}"
              data-optanongroupid="3">
              <p class="preference-menu-item">
                <button role="tab" aria-selected="false" aria-controls="功能-Cookie" id="功能-Cookie">功能 Cookie</button>
              </p>
            </li>
            <li class="menu-item-on menu-item-advertising" title="定向 Cookie"
              data-group="{&quot;ShowInPopup&quot;:true,&quot;Order&quot;:3,&quot;OptanonGroupId&quot;:4,&quot;Parent&quot;:null,&quot;GroupLanguagePropertiesSets&quot;:[{&quot;DefaultStatus&quot;:{&quot;Text&quot;:&quot;Active&quot;},&quot;GroupDescription&quot;:{&quot;Text&quot;:&quot;这些 Cookie 由广告合作伙伴通过我们的网站进行设置。这些公司可能利用 Cookie 构建您的兴趣分布图并向您展示其他网站上的相关广告。它们只需识别您的浏览器和设备便可发挥作用。如果您不允许使用这些 Cookie，您将不能体验不同网站上的定向广告。&quot;},&quot;GroupName&quot;:{&quot;Text&quot;:&quot;定向 Cookie&quot;},&quot;IsDntEnabled&quot;:false}],&quot;Cookies&quot;:[{&quot;Name&quot;:&quot;GoogleAdServingTest&quot;,&quot;Host&quot;:&quot;www.zaobao.com.sg&quot;,&quot;IsSession&quot;:true,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;GED_PLAYLIST_ACTIVITY&quot;,&quot;Host&quot;:&quot;www.zaobao.com.sg&quot;,&quot;IsSession&quot;:true,&quot;Length&quot;:0},{&quot;Name&quot;:&quot;__gads&quot;,&quot;Host&quot;:&quot;.zaobao.com.sg&quot;,&quot;IsSession&quot;:false,&quot;Length&quot;:729}],&quot;Purposes&quot;:[],&quot;CustomGroupId&quot;:null,&quot;GroupId&quot;:1772}"
              data-optanongroupid="4">
              <p class="preference-menu-item">
                <button role="tab" aria-selected="false" aria-controls="定向-Cookie" id="定向-Cookie">定向 Cookie</button>
              </p>
            </li>
            <li class="menu-item-moreinfo menu-item-off" title="更多信息">
              <p class="preference-menu-item"><a target="_blank" aria-label="更多信息"
                  href="https://cookiepedia.co.uk/giving-consent-to-cookies"
                  onclick="Optanon.TriggerGoogleAnalyticsEvent(&#39;OneTrust Cookie Consent&#39;, &#39;Preferences Cookie Policy&#39;);">更多信息</a>
              </p>
            </li>
          </ul>
        </div>
        <div>
          <div id="optanon-popup-body-right">
            <p role="heading" aria-level="2" class="legacy-preference-banner-title h2" aria-label="隐私偏好中心">隐私偏好中心</p>
            <div class="vendor-header-container">
              <p class="header-3" role="heading" aria-level="3"></p>
              <div id="optanon-popup-more-info-bar">
                <div class="optanon-status">
                  <div class="optanon-status-editable">
                    <form><span class="fieldset">
                        <p><input type="checkbox" value="check" id="chkMain" checked="checked"
                            class="legacy-group-status optanon-status-checkbox"><label for="chkMain">活动状态</label></p>
                      </span></form>
                  </div>
                  <div class="optanon-status-always-active optanon-status-on">
                    <p>始终处于活动状态</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="optanon-main-info-text" role="tabpanel"></div>
          </div>
        </div>
        <div class="optanon-bottom-spacer"></div>
      </div>
      <div id="optanon-popup-bottom">
        <a href="https://onetrust.com/poweredbyonetrust" target="_blank" rel="noopener">
          <div id="optanon-popup-bottom-logo" alt="OneTrust Website"
            style="background: url(https://cdn-apac.onetrust.com/skins/5.7.0/default_flat_bottom_two_button_black/v2/images/cookie-collective-top-bottom.png);width:155px;height:35px;"
            title="powered by OneTrust"></div>
        </a>
        <div class="optanon-button-wrapper optanon-save-settings-button optanon-close optanon-close-consent">
          <div class="optanon-white-button-left"></div>
          <div class="optanon-white-button-middle">
            <button title="保存设置" aria-label="保存设置"
              onclick="Optanon.TriggerGoogleAnalyticsEvent(&#39;OneTrust Cookie Consent&#39;, &#39;Preferences Save Settings&#39;);">保存设置</button>
          </div>
          <div class="optanon-white-button-right"></div>
        </div>
        <div class="optanon-button-wrapper optanon-allow-all-button optanon-allow-all" style="display: none;">
          <div class="optanon-white-button-left"></div>
          <div class="optanon-white-button-middle">
            <button title="全部允许" aria-label="全部允许"
              onclick="Optanon.TriggerGoogleAnalyticsEvent(&#39;OneTrust Cookie Consent&#39;, &#39;Preferences Allow All&#39;);">全部允许</button>
          </div>
          <div class="optanon-white-button-right"></div>
        </div>
      </div>
    </div>
  </div>

  <!-- Google Tag Manager (noscript) -->
  <noscript>
    <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-K4JWN4Q" height="0" width="0"
      style="display:none;visibility:hidden"></iframe>
  </noscript>
  <!-- End Google Tag Manager (noscript) -->
  <!-- Google Better Ads Implementation Start -->
  <div id="MyPageOverlay"></div>
  <div id="postitial_holder"></div>
  <script>
    if (window.canRunAds !== undefined && (show_topoverlay == 1 || (dfp_preview_ids != undefined && dfp_preview_ids.length != 0)) && screen.width > 740) {
      document.getElementById("MyPageOverlay").className = "overlayWhite";
      unloadScrollBars();
    }
  </script>

  <!-- Google Better Ads Implementation Ends -->

  <div id="block-block-54" class="block block-block">
    <div class="content">
      <link rel="stylesheet" type="text/css" href="./raw/style-2019-ext.css">
      <div class="avt avt-leaderboard avt-leaderboard1">
        <div class="advertisement">
          <div id="dfp-ad-lb1-wrapper" class="dfp-tag-wrapper">
            <div id="dfp-ad-lb1" class="dfp-tag-wrapper" data-google-query-id="CO-v3Kr_3OYCFYSlaAodD9wGEQ">
              <div id="google_ads_iframe_/5908/ZB_SG/lb1/znews/singapore_0__container__"
                style="border: 0pt none; display: inline-block; width: 728px; height: 90px;">
                <iframe frameborder="0" src="./raw/container.html"
                  id="google_ads_iframe_/5908/ZB_SG/lb1/znews/singapore_0" title="3rd party ad content" name=""
                  scrolling="no" marginwidth="0" marginheight="0" width="728" height="90" data-is-safeframe="true"
                  sandbox="allow-forms allow-pointer-lock allow-popups allow-popups-to-escape-sandbox allow-same-origin allow-scripts allow-top-navigation-by-user-activation"
                  data-google-container-id="1" style="border: 0px; vertical-align: bottom;"
                  data-load-complete="true"></iframe>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="wrap js-stickybit-parent js-stickybit-parent">

    <header class="header header-desktop js-is-sticky js-is-sticky--change" id="js-header-desktop"
      style="top: 0px; position: fixed;">
      <!-- topnavbar-primary - START-->
      <nav class="topnavbar topnavbar-primary" id="js-topnavbar-primary" style="height: auto;">
        <div class="topnavbar-content">
          <div class="topnavbar-featured">
            <div class="container no-padding-l">
              <div class="row align-items-center no-gutters">
                <div class="col-auto">
                  <a class="topnav-link logotype" href="https://www.zaobao.com.sg/"><img
                      src="./raw/zaobaosg-logo.svg" width="120"></a>
                </div>
                <div class="col-12 col-lg-auto d-none d-lg-block"><a class="topnav-link "
                    href="https://www.zaobao.com.sg/premium" target="_self">订户专区</a></div>
                <div class="col-12 col-lg-auto d-none d-lg-block"><a class="topnav-link "
                    href="https://www.zaobao.com.sg/publication/lian-he-wan-bao" target="_self">联合晚报</a></div>
                <div class="col-12 col-lg-auto d-none d-lg-block"><a class="topnav-link "
                    href="https://www.zaobao.com.sg/publication/xin-ming-ri-bao" target="_self">新明日报</a></div>
                <div class="col-12 col-lg-auto d-none d-lg-block"><a class="topnav-link "
                    href="https://zproperty.zaobao.com.sg/" target="_blank">早房</a></div>
                <div class="col-12 col-lg-auto d-none d-lg-block"><a class="topnav-link "
                    href="https://www.zaobao.com.sg/readersclub" target="_self">读者好康</a></div>
                <div class="col-12 col-lg-auto d-none d-lg-block"><a class="topnav-link "
                    href="https://www.zaobao.com.sg/epapers" target="_self">电子报</a></div>
                <div class="col-12 col-lg-auto d-none d-lg-block"><a class="topnav-link "
                    href="https://www.zaobao.com.sg/welcomecn" target="_self">国际版</a></div>
                <div class="col-auto ml-auto">
                  <div class="cta-holder cta-holder-subscribe" id="login-info"><span class="welcome">欢迎,&nbsp;</span>
                    <li class="nav-user user user-dropdown dropdown row no-gutters"><a href="javascript:;"
                        class="dropdown-toggle" name="login-user-name" data-toggle="dropdown"
                        aria-expanded="false"><span id="sph_user_name">pwlow@sph.com.sg</span></a>
                      <div class="col-12 dropdown-menu">
                        <div class="topnav-submenu">
                          <div class="topnav-link dropdown-item"><a class="mysph_user_name" href="javascript:;">更新密码</a>
                          </div>
                          <div class="topnav-link dropdown-item"><a class="mysph_logout" href="javascript:;">退出</a>
                          </div>
                        </div>
                      </div>
                    </li>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <!-- topnavbar-primary - END-->
      <!-- topnavbar-secondary - START-->
      <nav class="topnavbar topnavbar-secondary">
        <div class="topnavbar-content">
          <div class="topnavbar-news">
            <div class="container no-padding-l">
              <div class="row no-gutters align-items-center full-width">
                <div class="col-12 col-lg-auto col-animate">
                  <a class="topnav-link logo" href="https://www.zaobao.com.sg/">
                    <svg class="svg svg-logo" xmlns="https://www.w3.org/2000/svg"
                      xmlns:xlink="https://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px"
                      viewBox="0 0 131.271 131.271" enable-background="new 0 0 131.271 131.271" xml:space="preserve">
                      <script xmlns=""></script>
                      <g>
                        <g>
                          <path fill="#A50034"
                            d="M3.741,43.989h123.512C118.262,18.366,94.017,0,65.503,0C36.978,0,12.73,18.366,3.741,43.989z">
                          </path>
                          <path fill="#FF1339"
                            d="M0,65.496C0,73.254,1.357,80.694,3.835,87.6h123.322c2.478-6.906,3.834-14.35,3.834-22.104    c0-7.654-1.325-14.995-3.738-21.824H3.741C1.327,50.501,0,57.842,0,65.496z">
                          </path>
                          <path fill="#009AE0"
                            d="M65.503,130.99c28.409,0,52.586-18.101,61.654-43.39H3.835C12.908,112.889,37.085,130.99,65.503,130.99z">
                          </path>
                        </g>
                        <path opacity="0.2" enable-background="new    "
                          d="M71.017,130.591c0.03,0.052,0.063,0.096,0.091,0.146   c32.143-2.729,57.653-28.665,59.73-60.984c-0.277-0.274-0.547-0.579-0.838-0.838c-0.618-0.662-1.229-1.299-1.877-1.88   c-0.627-0.671-1.247-1.315-1.905-1.903c-0.619-0.663-1.231-1.302-1.883-1.883c-0.625-0.671-1.245-1.314-1.903-1.906   c-0.62-0.659-1.229-1.301-1.88-1.88c-0.625-0.669-1.245-1.313-1.904-1.898c-0.62-0.667-1.235-1.311-1.891-1.894   c-0.624-0.667-1.239-1.311-1.896-1.895c-0.619-0.667-1.233-1.309-1.886-1.892c-0.623-0.667-1.241-1.308-1.895-1.891   c-0.62-0.665-1.235-1.306-1.888-1.887c-0.626-0.671-1.242-1.317-1.9-1.903c-0.625-0.667-1.241-1.311-1.898-1.896   c-0.618-0.665-1.229-1.307-1.882-1.885c-0.623-0.667-1.239-1.311-1.893-1.895c-0.624-0.667-1.245-1.313-1.897-1.898   c-0.621-0.662-1.237-1.306-1.889-1.887c-0.625-0.669-1.246-1.314-1.902-1.903c-0.62-0.663-1.23-1.304-1.887-1.885   c-0.625-0.669-1.245-1.313-1.898-1.898c-0.616-0.66-1.227-1.294-1.873-1.872c-0.627-0.675-1.253-1.323-1.91-1.91   c-0.625-0.67-1.24-1.313-1.896-1.896c-1.796-1.925-3.503-3.689-6.388-4.206c-3.013-0.531-9.604,1.773-12.428,3.023   c-2.218,0.979-6.275,3.874-5.35,6.915c0.308,0.994,0.961,1.416,1.847,1.467c0.021,0.14,0.004,0.28,0.046,0.425   c0.155,0.504,0.428,0.823,0.747,1.063c-1.458,0.212-2.74,1.542-4.054,2.531c-0.394-0.444-0.825-0.839-1.292-1.145   c-0.532-0.739-1.171-1.415-1.889-1.889c-0.534-0.744-1.177-1.422-1.898-1.896c-0.532-0.738-1.171-1.418-1.89-1.891   c-0.531-0.741-1.175-1.421-1.896-1.896c-1.065-1.483-2.568-2.799-4.248-2.483c-2.163,0.404-3.805,2.995-4.797,4.74   c-2.526,4.382-3.528,11.055-2.2,15.987c1.053,3.847,3.072,6.683,3.398,10.6c0.3,3.493,1.284,5.406,3.121,6.458   c0.479,0.822,1.106,1.443,1.894,1.894c0.481,0.821,0.932,1.563,1.712,2.012c0.48,0.825,1.287,1.325,2.071,1.774   c0.482,0.824,1.11,1.441,1.896,1.889c0.479,0.826,1.104,1.446,1.893,1.896c0.082,0.141,0.15,0.304,0.241,0.432   c-3.13,0.963-6.706,1.317-8.563,1.675c-6.854,1.322-14.365-0.069-20.297,4.443c-2.409,1.843-2.036,6.195-0.3,8.438   c0.564,0.73,1.062,1.132,1.523,1.31">
                        </path>
                        <path opacity="0.2" enable-background="new    "
                          d="M67.227,126.801c0.118,0.2,0.23,0.411,0.369,0.591"></path>
                        <path opacity="0.2" enable-background="new    " d="M63.81,123.602"></path>
                        <path opacity="0.2" enable-background="new    " d="M61.915,121.712"></path>
                        <path opacity="0.2" enable-background="new    " d="M59.657,119.231"></path>
                        <path opacity="0.2" enable-background="new    " d="M46.774,106.566"></path>
                        <path fill="#FFFFFF"
                          d="M88.584,27.546c-2.807-2.6-4.541-5.666-8.505-6.372c-3.013-0.531-9.604,1.773-12.428,3.023   c-2.218,0.979-6.275,3.874-5.35,6.915c1.646,5.344,13.308-5.711,17.325-3.794c2.421,1.146,2.399,4.683,2.003,6.856   c-0.76,4.312-2.434,8.88-5.189,12.313c-1.428,1.777-3.578,2.435-4.984,3.946c-1.323,1.423-0.98,3.267-3.017,4.212   c-2.609,1.212-10.689,0.005-8.087-4.474c1.729-2.967,6.14-4.201,8.325-6.979c2.537-3.18,1.796-5.975-1.369-8.412   c-2.98-2.309-5.099,1.262-7.475,2.501c-4.926,2.547-5.272-4.126-6.558-7.188c-0.812-1.945-2.967-5.17-5.502-4.697   c-2.161,0.404-3.805,2.995-4.797,4.74c-2.526,4.382-3.528,11.055-2.199,15.987c1.052,3.847,3.071,6.683,3.397,10.6   c0.509,5.938,2.921,7.386,8.341,7.612c2.701,0.116,10.552-0.889,10.365,3.491c-0.227,5.512-10.743,6.214-14.442,6.923   c-6.854,1.322-14.365-0.069-20.297,4.443c-2.409,1.843-2.036,6.195-0.3,8.438c2.309,2.994,3.491,0.583,5.91-1.104   c3.104-2.155,7.646-1.806,11.123-3.135c4.222-1.624,8.411-1.854,12.896-2.521c1.61-0.236,4.229-1.357,5.817-0.615   c4.313,2.036,2.65,13.898,2.15,17.475c-0.437,3.102-1.883,6.166-1.036,9.341c0.832,3.167,3.614,6.861,6.832,3.625   c2.769-2.82,3.61-8.471,3.725-12.295c0.182-5.563-0.91-11.132-0.569-16.704c0.084-1.297,0.254-2.93,1.329-3.844   c0.737-0.628,1.787-0.897,2.639-1.376c2.896-1.631,13.063-0.954,14.996-3.825c1.229-1.826,0.519-4.366-1.361-5.444   c-2.197-1.258-4.949-1.124-7.349-0.559c-1.542,0.354-9.075,4.367-10.295,2.705c-1.441-1.969-1.733-6.765,0.529-8.146   c2.521-1.563,6.528,0.429,8.896-2.254c1.451-1.661,0.97-4.366,1.496-6.367c1.759-6.722,7.428-10.976,6.327-18.706   C91.541,31.392,90.431,29.262,88.584,27.546">
                        </path>
                      </g>
                    </svg>
                  </a>
                </div>
                <div class="col-12 col-lg">
                  <div class="row align-items-center row no-gutters">
                    <div class="col-12 col-lg-auto dropdown"><a class="topnav-link is-active"
                        href="https://www.zaobao.com.sg/znews"><span class="dropdown-toggle" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false" id="topNavBarDropDown-47727">新闻</span></a>
                      <div class="dropdown-menu" aria-labelledby="topNavBarDropDown-47727">
                        <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/realtime">即时</a><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/znews/singapore">新加坡</a><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/znews/international">国际</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/znews/greater-china">中港台</a><a
                            class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/znews/sea">东南亚</a> </div>
                      </div>
                    </div>
                    <div class="col-12 col-lg-auto dropdown"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/zfinance"><span class="dropdown-toggle" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false" id="topNavBarDropDown-47734">财经</span></a>
                      <div class="dropdown-menu" aria-labelledby="topNavBarDropDown-47734">
                        <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                            href="https://stock.zaobao.com.sg/">新加坡股市</a><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zfinance/realtime">即时</a><a
                            class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zfinance/news">新闻</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zfinance/invest">投资理财</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zfinance/real-estate">房产</a><a
                            class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zfinance/sme">中小企业</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zfinance/personalities">财经人物</a> </div>
                      </div>
                    </div>
                    <div class="col-12 col-lg-auto dropdown"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/zopinions"><span class="dropdown-toggle" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false" id="topNavBarDropDown-47743">言论</span></a>
                      <div class="dropdown-menu" aria-labelledby="topNavBarDropDown-47743">
                        <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zopinions/editorial">社论</a><a
                            class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zopinions/views">评论</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zopinions/opinions">想法</a><a
                            class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zopinions/talk">交流站</a>
                        </div>
                      </div>
                    </div>
                    <div class="col-12 col-lg-auto dropdown"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/zentertainment"><span class="dropdown-toggle"
                          data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                          id="topNavBarDropDown-47748">娱乐</span></a>
                      <div class="dropdown-menu" aria-labelledby="topNavBarDropDown-47748">
                        <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zentertainment/celebs">明星</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zentertainment/movies-and-tv">影视</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zentertainment/music">音乐</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zentertainment/k-pop">韩流</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zentertainment/giveaway">送礼</a> </div>
                      </div>
                    </div>
                    <div class="col-12 col-lg-auto dropdown"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/zlifestyle"><span class="dropdown-toggle" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false" id="topNavBarDropDown-47758">生活</span></a>
                      <div class="dropdown-menu" aria-labelledby="topNavBarDropDown-47758">
                        <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/food">美食</a><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/trending">热门</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/car-gadget">汽车与科技</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/powerup">生活补给站</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/life-hacks">生活贴士</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/fashion">时尚</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/travel">旅行</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/familynlove">心事家事</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/culture">文化/艺术</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zlifestyle/columns">专栏</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/package/latenightreads">深夜好读</a><a
                            class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/horoscope">星座运程</a> </div>
                      </div>
                    </div>
                    <div class="col-12 col-lg-auto dropdown"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/zvideos"><span class="dropdown-toggle" data-toggle="dropdown"
                          aria-haspopup="true" aria-expanded="false" id="topNavBarDropDown-50059">视频</span></a>
                      <div class="dropdown-menu" aria-labelledby="topNavBarDropDown-50059">
                        <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zvideos/news">新闻</a><a class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zvideos/entertainment">娱乐</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zvideos/lifestyle">生活</a><a
                            class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/video-series">系列节目</a><a
                            class="topnav-link dropdown-item"
                            href="https://www.zaobao.com.sg/zvideos/zbschools">早报校园</a><a
                            class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/livestream">直播</a> </div>
                      </div>
                    </div>
                    <div class="col-12 col-lg-auto"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/zlifestyle/beauty-health">保健/美容</a></div>
                    <div class="col-12 col-lg-auto"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/znews/sports">体育</a></div>
                    <div class="col-12 col-lg-auto"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/special/report">专题</a></div>
                    <div class="col-12 col-lg-auto"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/interactive-graphics">互动新闻</a></div>
                    <div class="col-12 col-lg-auto"><a class="topnav-link "
                        href="https://www.zaobao.com.sg/podcast">早报播客</a></div>
                  </div>
                </div>
                <div class="col-12 col-lg-auto">
                  <span id="js-topnavbar-search">
                    <svg class="svg svg-icon-search" xmlns="https://www.w3.org/2000/svg" width="22" height="22"
                      viewBox="0 0 22 22">
                      <g fill="#FFF" fill-rule="evenodd">
                        <path
                          d="M8.643 15.64a7.004 7.004 0 0 1-6.997-6.997 7.004 7.004 0 0 1 6.997-6.997 7.004 7.004 0 0 1 6.996 6.997 7.004 7.004 0 0 1-6.996 6.996M8.643 0C3.877 0 0 3.877 0 8.643c0 4.766 3.877 8.643 8.643 8.643 4.766 0 8.643-3.877 8.643-8.643C17.286 3.877 13.409 0 8.643 0">
                        </path>
                        <path
                          d="M20 19.433l-.567.567c-.102.003-.24-.039-.283-.081L15 15.767l.767-.767 4.15 4.152c.046.047.088.173.083.281m1.423-1.222l-4.733-4.74A1.653 1.653 0 0 0 15.523 13c-.442 0-.867.172-1.166.471l-.875.876a1.655 1.655 0 0 0 0 2.335l4.733 4.74c.362.362.91.578 1.467.578.28 0 .553-.056.788-.161l.07-.031 1.182-1.145.117-.194c.324-.725.145-1.696-.416-2.258">
                        </path>
                      </g>
                    </svg>
                  </span>
                  <div class="cta-holder cta-holder-subscribe"><a class="cta cta-minw cta-bordered cta-white"
                      href="https://www.sphsubscription.com.sg/eshop/?r=products/newsubscriptionpackages&amp;pcode=zb&amp;utm_campaign=zb_subcription&amp;utm_medium=sph-publication&amp;utm_source=zb&amp;lang=zh&amp;utm_content=homepageheader"
                      target="_blank">订阅</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <!-- topnavbar-secondary - END-->
      <!-- topnavbar-tertiary - START-->
      <nav class="topnavbar topnavbar-tertiary topnavbar-tertiary" id="js-topnavbar-tertiary" style="height: auto;">
        <div class="topnavbar-content">
          <div class="topnavbar-miscellaneous">
            <ul class="topnavbar-miscellaneous-list">
              <!-- Cxense Module: NewTheme - Top news in homepage(3 col) -->
              <div id="cx_b8e0544c23cc8cca5412a636624408c57aeba617">
                <li>
                  <div class="miscellaneous-cta">
                    <div class="miscellaneous-img">
                      <a class="article-type-img-link" id="cXLinkIdk4s7f7dfprk8msot"
                        href="https://www.zaobao.com.sg/znews/singapore/story20191230-1017076"><img
                          src="./raw/qi_xun_bo_.jpg"></a>

                      <a href="https://www.zaobao.com.sg/znews/singapore">
                        <h2>新加坡</h2>
                      </a>

                      <div class="label label-premium small">订户</div>

                    </div>
                    <div class="miscellaneous-text">

                      <a href="https://www.zaobao.com.sg/znews/singapore">
                        <h2>新加坡</h2>
                      </a>

                      <a id="cXLinkIdk4s7f7df2lozyxww"
                        href="https://www.zaobao.com.sg/znews/singapore/story20191230-1017076" target="_top">
                        <p>与印尼看护育一女 妻离婚后卖屋子七旬伯无家可归</p>
                      </a>
                    </div>
                  </div>
                </li>

                <li>
                  <div class="miscellaneous-cta">
                    <div class="miscellaneous-img">
                      <a class="article-type-img-link" id="cXLinkIdk4s7f7dfyuunesl5"
                        href="https://www.zaobao.com.sg/znews/singapore/story20191230-1017085"><img
                          src="./raw/201901230-pg4-pic1.jpg"></a>

                      <a href="https://www.zaobao.com.sg/znews/singapore">
                        <h2>新加坡</h2>
                      </a>

                    </div>
                    <div class="miscellaneous-text">

                      <a href="https://www.zaobao.com.sg/znews/singapore">
                        <h2>新加坡</h2>
                      </a>

                      <a id="cXLinkIdk4s7f7dfm5en7gdm"
                        href="https://www.zaobao.com.sg/znews/singapore/story20191230-1017085" target="_top">
                        <p>【幸运商业中心恐怖车祸】庆生遇浩劫 姐妹花阴阳相隔</p>
                      </a>
                    </div>
                  </div>
                </li>
              </div>
              <script type="text/javascript">
                var cX = window.cX = window.cX || {};
                cX.callQueue = cX.callQueue || [];
                cX.CCE = cX.CCE || {};
                cX.CCE.callQueue = cX.CCE.callQueue || [];
                cX.CCE.callQueue.push(['run', {
                  widgetId: 'b8e0544c23cc8cca5412a636624408c57aeba617',
                  targetElementId: 'cx_b8e0544c23cc8cca5412a636624408c57aeba617'
                }]);
              </script>
              <!-- Cxense Module End -->

              <li>
                <div class="miscellaneous-cta">
                  <div class="miscellaneous-img">
                    <img typeof="foaf:Image" src="./raw/image.jpg" width="480"
                      height="300" alt=""> <a href="https://www.zaobao.com.sg/zvideos/entertainment">
                      <h2>娱乐</h2>
                    </a> </div>
                  <div class="miscellaneous-text">
                    <a href="https://www.zaobao.com.sg/zvideos/entertainment">
                      <h2>娱乐</h2>
                    </a>
                    <a href="https://www.zaobao.com.sg/zvideos/entertainment/story20191228-1016697">
                      <p>郑伊健来新开唱 与歌迷重温经典</p>
                    </a>
                  </div>
                </div>
              </li>

              <li>

                <div class="miscellaneous-cta">
                  <div class="miscellaneous-img"
                    style="background-image: url(/sites/all/themes/zb2019/dist/images/horoscope/sprite-horoscope-256.png)">
                    <a href="https://www.zaobao.com.sg/horoscope?ref=recommendation">
                      <h2> 星座运程</h2>
                    </a>
                  </div>

                  <div class="miscellaneous-text">
                    <a href="https://www.zaobao.com.sg/horoscope?ref=recommendation">
                      <h2> 星座运程</h2>
                    </a>
                    <a href="https://www.zaobao.com.sg/horoscope/sagittarius?ref=recommendation">
                      <p><strong>射手座</strong>本周你脑子里可能会有很多稀奇古怪的想法，...</p>
                    </a>
                  </div>
                </div>
              </li>

            </ul>
          </div>
        </div>
      </nav>
      <!-- topnavbar-tertiary - END-->
    </header>
    <header class="header header-mobile js-is-sticky js-is-sticky--change" id="js-header-mobile"
      style="top: 0px; position: fixed;">
      <!-- topnavbar-mobile - START-->
      <nav class="topnavbar topnavbar-mobile">
        <div class="topnavbar-content">
          <div class="topnavbar-header js-topnavbar-header">
            <div class="mobile-text-links">
              <ul class="mobile-text-links-list">
                <div
                  class="view view-zb2016-freefrom view-id-zb2016_freefrom view-display-id-mobile_quicklinks_common view-dom-id-6706c3950ddf5feede5b8d230c799622">

                  <div class="view-content">
                    <div>
                      <div about="/content/quicklinks-mobile-994073" typeof="sioc:Item foaf:Document"
                        class="ds-1col node node-zb2016-freeform node-promoted view-mode-full  clearfix">

                        <!--li class="mobile-text-links-item"><a href="/keywords/urban-farmers?utm_source=website-new&utm_medium=quicklinkmobile" target="_blank">城市农夫</a></li-->

                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/zlifestyle/familynlove/story20191118-1006172?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">怕输心理好还是不好？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/package/latenightreads?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">深夜好读</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/zlifestyle/familynlove/story20191216-1013607?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">在团队里如何与人为善？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/zopinions/editorial/story20191213-1012965?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">你的脸时时刻刻都被刷了</a></li>
                        <li class="mobile-text-links-item is-active"><a
                            href="https://www.zaobao.com.sg/zlifestyle/beauty-health?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">关注: 美容与保健</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/zentertainment/celebs/story20191221-1014998?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">李连杰是怎么教育女儿的</a></li>
                        <style>
                          .topnavbar-mobile .topnavbar-header .mobile-text-links-item a {
                            font-size: 13px !important;
                            font-size: 1.3rem !important;
                            font-weight: 500;
                            letter-spacing: 0;
                            line-height: 1.2;
                            display: inline-block;
                            width: auto !important;
                          }

                          .mobile-text-links-item a {
                            display: block;
                          }

                          #skinning {
                            width: 100%;
                          }

                          img.img-responsive {
                            width: 100%;
                          }
                        </style>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/homeless-in-singapore/?utm_source=website-new&amp;utm_medium=quicklinkmobile&amp;utm_content=headline4"
                            target="_blank">【关注】城市的“隐形人”</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/readersclub?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">读好报 享好康 快来享受各种优惠！</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/zopinions/opinions/story20191215-1013421?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">“我的判断一定是对的”</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/zopinions/talk/story20191211-1012384?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">新加坡学生对失败的恐惧有多大？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/keywords/zha-pian?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">你能分辨诈骗吗?</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/homeless-in-singapore/?utm_source=website-new&amp;utm_medium=quicklinkmobile&amp;utm_content=headline3"
                            target="_blank">什么导致他们错失政府给予的福利？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/keywords/xing-yun-da-shi-dian-zhi-hui?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">每日的养生智慧</a></li>
                        <script>
                          var grd = $('.view-mode-full');
                          var imgs = grd.children();
                          imgs.sort(function () {
                            return (Math.round(Math.random()) - 0.5);
                          });
                          grd.remove('li.mobile-text-links-item');
                          for (var i = 0; i < imgs.length; i++) grd.append(imgs[i]);
                        </script>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/homeless-in-singapore/?utm_source=website-new&amp;utm_medium=quicklinkmobile&amp;utm_content=headline7"
                            target="_blank">”谢谢你配合我们玩这个游戏“</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/homeless-in-singapore/?utm_source=website-new&amp;utm_medium=quicklinkmobile&amp;utm_content=headline5"
                            target="_blank">不再以天为被以地为床</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/byline/luo-zhen-ling?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">跟随罗瑱玲看娱乐听音乐</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/homeless-in-singapore/?utm_source=website-new&amp;utm_medium=quicklinkmobile&amp;utm_content=headline8"
                            target="_blank">给了他们一个家只是起点</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/homeless-in-singapore/?utm_source=website-new&amp;utm_medium=quicklinkmobile&amp;utm_content=headline2"
                            target="_blank">什么原因导致露宿者拒绝离开街头？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/homeless-in-singapore/?utm_source=website-new&amp;utm_medium=quicklinkmobile&amp;utm_content=headline6"
                            target="_blank">街头露宿者的一场游戏一场梦</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/zopinions/talk/story20191217-1013903?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">“养”而忽略了“育”</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/horoscope?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">今日本周你的运程如何？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/keywords/reading-newspapers-with-the-elderly/?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">早晚陪你看报纸活动详情</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/news/fukan/gen?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">世代的形形色色</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/keywords/discovery-walk?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">踏出健康的第一步 出去走走</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/podcast?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">5档早报播客节目任你挑</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/homeless-in-singapore/?utm_source=website-new&amp;utm_medium=quicklinkmobile&amp;utm_content=headline1"
                            target="_blank">他们为何露宿在外？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/zopinions/talk/story20191218-1014199?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">看到AED的白箱子 你知道怎么用吗？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/income-calculator/?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">你觉得你的收入比别人低？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://www.zaobao.com.sg/keywords/wanbao-food-search?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">快来发掘地铁站附近的美食</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/homeless-in-singapore/?utm_source=website-new&amp;utm_medium=quicklinkmobile&amp;utm_content=headline9"
                            target="_blank">为什么会有1000个人露宿街头？</a></li>
                        <li class="mobile-text-links-item"><a
                            href="https://interactive.zaobao.com/2019/award-winning-hawker-food/?utm_source=website-new&amp;utm_medium=quicklinkmobile"
                            target="_blank">轻松寻找得奖小贩美食</a></li>
                      </div>

                    </div>
                  </div>

                </div>
              </ul>
            </div>
            <a class="logotype" href="https://www.zaobao.com.sg/"><img
                src="./raw/zaobaosg-logo.svg"
                style="width:100%; max-width:90px; max-height:40px;"></a>
            <div class="container">
              <div class="row align-items-center">
                <div class="col-auto">
                  <button class="ham js-ham" type="button" data-toggle="collapse"
                    data-target="#topnavbar-toggler-mobile" aria-controls="topnavbar-toggler-mobile"
                    aria-expanded="false" aria-label="Toggle navigation mobile">
                    <div class="hamburger">
                      <div></div>
                      <div></div>
                      <div></div>
                    </div>
                  </button>
                </div>
                <div class="col-auto ml-auto">
                  <div class="cta-holder cta-holder-subscribe"><a class="cta cta-minw cta-solid cta-gradient-red"
                      href="https://www.sphsubscription.com.sg/eshop/?r=products/newsubscriptionpackages&amp;pcode=zb&amp;utm_campaign=zb_subcription&amp;utm_medium=sph-publication&amp;utm_source=zb&amp;lang=zh&amp;utm_content=homepageheader"
                      target="_blank">订阅</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="topnavbar-body js-topnavbar-body">
            <div class="collapse navbar-collapse" id="topnavbar-toggler-mobile">
              <div class="topnavbar-search">
                <div class="container">
                  <div class="row">
                    <div class="col-12">
                      <form method="post" id="search-form-mobile" class="hideSubmitButton-processed">
                        <div class="searchbar">
                          <input name="keys" id="zbSearchKeyword-mobile" type="search" placeholder="输入搜索词">
                          <div id="search-errormsg-mb" style="display: none;">请输入关键词</div>
                          <svg class="svg svg-icon-search" xmlns="https://www.w3.org/2000/svg" width="22" height="22"
                            viewBox="0 0 22 22">
                            <g fill="#FFF" fill-rule="evenodd">
                              <path
                                d="M8.643 15.64a7.004 7.004 0 0 1-6.997-6.997 7.004 7.004 0 0 1 6.997-6.997 7.004 7.004 0 0 1 6.996 6.997 7.004 7.004 0 0 1-6.996 6.996M8.643 0C3.877 0 0 3.877 0 8.643c0 4.766 3.877 8.643 8.643 8.643 4.766 0 8.643-3.877 8.643-8.643C17.286 3.877 13.409 0 8.643 0">
                              </path>
                              <path
                                d="M20 19.433l-.567.567c-.102.003-.24-.039-.283-.081L15 15.767l.767-.767 4.15 4.152c.046.047.088.173.083.281m1.423-1.222l-4.733-4.74A1.653 1.653 0 0 0 15.523 13c-.442 0-.867.172-1.166.471l-.875.876a1.655 1.655 0 0 0 0 2.335l4.733 4.74c.362.362.91.578 1.467.578.28 0 .553-.056.788-.161l.07-.031 1.182-1.145.117-.194c.324-.725.145-1.696-.416-2.258">
                              </path>
                            </g>
                          </svg>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
              <div class="topnavbar-fontsizing">
                <div class="container">
                  <div class="row">
                    <div class="col-12">
                      <div class="topnavbar-fontsizing-title">
                        <p>字体大小:</p>
                      </div>
                      <ul class="fontsizing-range-list js-fontsizing">
                        <li data-fontsize="10" class="is-active">
                          <div class="cta cta-bordered cta-white cta-solid cta-solid-white">小</div>
                        </li>
                        <li data-fontsize="11">
                          <div class="cta cta-bordered cta-white">中</div>
                        </li>
                        <li data-fontsize="12">
                          <div class="cta cta-bordered cta-white">大</div>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
              <div class="divider"></div>
              <div class="topnavbar-featured js-fontsized" style="font-size: 10px;">
                <div class="container">
                  <div class="row">
                    <div class="col-12">
                      <div class="row align-items-center justify-content-center">
                        <div class="col-auto"><a class="topnav-link " href="https://www.zaobao.com.sg/premium"
                            target="_self">订户专区</a></div>
                        <div class="col-auto"><a class="topnav-link "
                            href="https://www.zaobao.com.sg/publication/lian-he-wan-bao" target="_self">联合晚报</a></div>
                        <div class="col-auto"><a class="topnav-link "
                            href="https://www.zaobao.com.sg/publication/xin-ming-ri-bao" target="_self">新明日报</a></div>
                        <div class="col-auto"><a class="topnav-link " href="https://zproperty.zaobao.com.sg/"
                            target="_blank">早房</a></div>
                        <div class="col-auto"><a class="topnav-link " href="https://www.zaobao.com.sg/readersclub"
                            target="_self">读者好康</a></div>
                        <div class="col-auto"><a class="topnav-link " href="https://www.zaobao.com.sg/epapers"
                            target="_self">电子报</a></div>
                        <div class="col-auto"><a class="topnav-link " href="https://www.zaobao.com.sg/welcomecn"
                            target="_self">国际版</a></div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="divider"></div>
              <div class="topnavbar-news js-fontsized" style="font-size: 10px;">
                <ul class="topnavbar-news-list">
                  <li><a class="topnav-link" href="https://www.zaobao.com.sg/">首页</a></li>
                  <li class="dropdown row no-gutters">
                    <div class="col-auto"><a class="topnav-link topnav-link-listing"
                        href="https://www.zaobao.com.sg/znews">新闻</a></div>
                    <div class="col dropdown-toggle is-active" data-toggle="dropdown" aria-haspopup="true"
                      aria-expanded="false" id="topNavBarDropDown-49950 新闻 47727">
                      <div class="fas fa-caret-down"></div>
                    </div>
                    <div class="col-12 dropdown-menu" aria-labelledby="topNavBarDropDown-49950 新闻 47727">
                      <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/realtime">即时</a><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/znews/singapore">新加坡</a><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/znews/international">国际</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/znews/greater-china">中港台</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/znews/sea">东南亚</a> </div>
                    </div>
                  </li>
                  <li class="dropdown row no-gutters">
                    <div class="col-auto"><a class="topnav-link topnav-link-listing"
                        href="https://www.zaobao.com.sg/zfinance">财经</a></div>
                    <div class="col dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                      id="topNavBarDropDown-49951 财经 47734">
                      <div class="fas fa-caret-down"></div>
                    </div>
                    <div class="col-12 dropdown-menu" aria-labelledby="topNavBarDropDown-49951 财经 47734">
                      <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                          href="https://stock.zaobao.com.sg/">新加坡股市</a><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zfinance/realtime">即时</a><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zfinance/news">新闻</a><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zfinance/invest">投资理财</a><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zfinance/real-estate">房产</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zfinance/sme">中小企业</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zfinance/personalities">财经人物</a> </div>
                    </div>
                  </li>
                  <li class="dropdown row no-gutters">
                    <div class="col-auto"><a class="topnav-link topnav-link-listing"
                        href="https://www.zaobao.com.sg/zopinions">言论</a></div>
                    <div class="col dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                      id="topNavBarDropDown-49952 言论 47743">
                      <div class="fas fa-caret-down"></div>
                    </div>
                    <div class="col-12 dropdown-menu" aria-labelledby="topNavBarDropDown-49952 言论 47743">
                      <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zopinions/editorial">社论</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zopinions/views">评论</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zopinions/opinions">想法</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zopinions/talk">交流站</a>
                      </div>
                    </div>
                  </li>
                  <li class="dropdown row no-gutters">
                    <div class="col-auto"><a class="topnav-link topnav-link-listing"
                        href="https://www.zaobao.com.sg/zentertainment">娱乐</a></div>
                    <div class="col dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                      id="topNavBarDropDown-49953 娱乐 47748">
                      <div class="fas fa-caret-down"></div>
                    </div>
                    <div class="col-12 dropdown-menu" aria-labelledby="topNavBarDropDown-49953 娱乐 47748">
                      <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zentertainment/celebs">明星</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zentertainment/movies-and-tv">影视</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zentertainment/music">音乐</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zentertainment/k-pop">韩流</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zentertainment/giveaway">送礼</a> </div>
                    </div>
                  </li>
                  <li class="dropdown row no-gutters">
                    <div class="col-auto"><a class="topnav-link topnav-link-listing"
                        href="https://www.zaobao.com.sg/zlifestyle">生活</a></div>
                    <div class="col dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                      id="topNavBarDropDown-49954 生活 47758">
                      <div class="fas fa-caret-down"></div>
                    </div>
                    <div class="col-12 dropdown-menu" aria-labelledby="topNavBarDropDown-49954 生活 47758">
                      <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zlifestyle/food">美食</a><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zlifestyle/trending">热门</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zlifestyle/car-gadget">汽车与科技</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zlifestyle/powerup">生活补给站</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zlifestyle/life-hacks">生活贴士</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zlifestyle/fashion">时尚</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zlifestyle/travel">旅行</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zlifestyle/familynlove">心事家事</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zlifestyle/culture">文化/艺术</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zlifestyle/columns">专栏</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/package/latenightreads">深夜好读</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/horoscope">星座运程</a> </div>
                    </div>
                  </li>
                  <li class="dropdown row no-gutters">
                    <div class="col-auto"><a class="topnav-link topnav-link-listing"
                        href="https://www.zaobao.com.sg/zvideos">视频</a></div>
                    <div class="col dropdown-toggle " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"
                      id="topNavBarDropDown-49955 视频 50059">
                      <div class="fas fa-caret-down"></div>
                    </div>
                    <div class="col-12 dropdown-menu" aria-labelledby="topNavBarDropDown-49955 视频 50059">
                      <div class="topnav-submenu"><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zvideos/news">新闻</a><a class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zvideos/entertainment">娱乐</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/zvideos/lifestyle">生活</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/video-series">系列节目</a><a
                          class="topnav-link dropdown-item"
                          href="https://www.zaobao.com.sg/zvideos/zbschools">早报校园</a><a
                          class="topnav-link dropdown-item" href="https://www.zaobao.com.sg/livestream">直播</a> </div>
                    </div>
                  </li>
                  <li><a class="topnav-link " href="https://www.zaobao.com.sg/zlifestyle/beauty-health">保健/美容</a></li>
                  <li><a class="topnav-link " href="https://www.zaobao.com.sg/znews/sports">体育</a></li>
                  <li><a class="topnav-link " href="https://www.zaobao.com.sg/special/report">专题</a></li>
                  <li><a class="topnav-link " href="https://www.zaobao.com.sg/interactive-graphics">互动新闻</a></li>
                  <li><a class="topnav-link " href="https://www.zaobao.com.sg/podcast">早报播客</a></li>
                </ul>
              </div>
              <div class="divider"></div>
              <div class="topnavbar-login" id="login-info-mobile"><span class="welcome">欢迎,&nbsp;</span>
                <li class="nav-user user user-dropdown dropdown row no-gutters"><a href="javascript:;"
                    class="dropdown-toggle" name="login-user-name" data-toggle="dropdown" aria-expanded="false"><span
                      id="sph_user_name">pwlow@sph.com.sg</span></a>
                  <div class="col-12 dropdown-menu">
                    <div class="topnav-submenu">
                      <div class="topnav-link dropdown-item"><a class="mysph_user_name" href="javascript:;">更新密码</a>
                      </div>
                      <div class="topnav-link dropdown-item"><a class="mysph_logout" href="javascript:;">退出</a></div>
                    </div>
                  </div>
                </li>
              </div>
            </div>
          </div>
        </div>
      </nav>
      <!-- topnavbar-mobile - END-->
      <!-- topnavbar-tertiary - START-->
      <nav class="topnavbar topnavbar-tertiary topnavbar-tertiary" id="js-topnavbar-tertiary" style="height: auto;">
        <div class="topnavbar-content">
          <div class="topnavbar-miscellaneous">
            <ul class="topnavbar-miscellaneous-list">
              <!-- Cxense Module:  NewTheme - Top news in homepage Mobile(3 col) -->
              <div id="cx_3f9fb4c1cc364bb81aaf2f7560fa388934075f0a">
                <li>
                  <div class="miscellaneous-cta">
                    <div class="miscellaneous-img">
                      <img src="./raw/qi_xun_bo_.jpg">

                      <a href="https://www.zaobao.com.sg/znews/singapore">
                        <h2>新加坡</h2>
                      </a>

                      <div class="label label-premium small">订户</div>

                    </div>
                    <div class="miscellaneous-text">

                      <a href="https://www.zaobao.com.sg/znews/singapore">
                        <h2>新加坡</h2>
                      </a>

                      <a id="cXLinkIdk4s7f73qhr1zqptd"
                        href="https://www.zaobao.com.sg/znews/singapore/story20191230-1017076" target="_top">
                        <p>与印尼看护育一女 妻离婚后卖屋子七旬伯无家可归</p>
                      </a>
                    </div>
                  </div>
                </li>

                <li>
                  <div class="miscellaneous-cta">
                    <div class="miscellaneous-img">
                      <img src="./raw/201901230-pg4-pic1.jpg">

                      <a href="https://www.zaobao.com.sg/znews/singapore">
                        <h2>新加坡</h2>
                      </a>

                    </div>
                    <div class="miscellaneous-text">

                      <a href="https://www.zaobao.com.sg/znews/singapore">
                        <h2>新加坡</h2>
                      </a>

                      <a id="cXLinkIdk4s7f73qp7xwd5ov"
                        href="https://www.zaobao.com.sg/znews/singapore/story20191230-1017085" target="_top">
                        <p>【幸运商业中心恐怖车祸】庆生遇浩劫 姐妹花阴阳相隔</p>
                      </a>
                    </div>
                  </div>
                </li>
              </div>
              <script type="text/javascript">
                var cX = window.cX = window.cX || {};
                cX.callQueue = cX.callQueue || [];
                cX.CCE = cX.CCE || {};
                cX.CCE.callQueue = cX.CCE.callQueue || [];
                cX.CCE.callQueue.push(['run', {
                  widgetId: '3f9fb4c1cc364bb81aaf2f7560fa388934075f0a',
                  targetElementId: 'cx_3f9fb4c1cc364bb81aaf2f7560fa388934075f0a'
                }]);
              </script>
              <!-- Cxense Module End -->

              <li>
                <div class="miscellaneous-cta">
                  <div class="miscellaneous-img">
                    <img typeof="foaf:Image" src="./raw/image.jpg" width="480"
                      height="300" alt=""> <a href="https://www.zaobao.com.sg/zvideos/entertainment">
                      <h2>娱乐</h2>
                    </a> </div>
                  <div class="miscellaneous-text">
                    <a href="https://www.zaobao.com.sg/zvideos/entertainment">
                      <h2>娱乐</h2>
                    </a>
                    <a href="https://www.zaobao.com.sg/zvideos/entertainment/story20191228-1016697">
                      <p>郑伊健来新开唱 与歌迷重温经典</p>
                    </a>
                  </div>
                </div>
              </li>

              <li>

                <div class="miscellaneous-cta">
                  <div class="miscellaneous-img"
                    style="background-image: url(/sites/all/themes/zb2019/dist/images/horoscope/sprite-horoscope-256.png)">
                    <a href="https://www.zaobao.com.sg/horoscope?ref=recommendation">
                      <h2> 星座运程</h2>
                    </a>
                  </div>

                  <div class="miscellaneous-text">
                    <a href="https://www.zaobao.com.sg/horoscope?ref=recommendation">
                      <h2> 星座运程</h2>
                    </a>
                    <a href="https://www.zaobao.com.sg/horoscope/taurus?ref=recommendation">
                      <p><strong>金牛座</strong>虽然你很容易坚持下来做一件事，...</p>
                    </a>
                  </div>
                </div>
              </li>

            </ul>
          </div>
        </div>
      </nav>
      <!-- topnavbar-tertiary - END-->
    </header>
    <div class="js-stickybits-usefixed-spacer" style="height: 45px;"></div>

    <!-- ### Content Wrapper - START ### -->
    <main class="main">

      <div id="skinning">
        <div id="dfp-ad-skinning-wrapper" class="dfp-tag-wrapper">
          <div id="dfp-ad-skinning" class="dfp-tag-wrapper" data-google-query-id="CKqR2Kr_3OYCFYSlaAodD9wGEQ"
            style="display: none;">
            <script type="text/javascript">
              googletag.cmd.push(function () {
                googletag.display("dfp-ad-skinning");
              });
            </script>
            <div id="google_ads_iframe_/5908/ZB_SG/skinning/znews/singapore_0__container__"
              style="border: 0pt none; width: 1px; height: 1px;"></div>
          </div>
        </div>
      </div>

      <!-- section - START-->
      <section class="sec">
        <div class="container">
          <div class="row">

            <div class="col-12 col-lg-1 js-stickybit-parent">
              <style>
                .tooltipster-shadow {
                  border-radius: 5px;
                  background: #fff;
                  box-shadow: 0px 0px 14px rgba(0, 0, 0, 0.3);
                  color: #2c2c2c;
                }

                .tooltipster-shadow .tooltipster-content {
                  font-family: 'Arial', sans-serif;
                  font-size: 14px;
                  line-height: 16px;
                  padding: 8px 10px;
                }

                .tooltipster-text-bold {
                  font-size: 12px;
                  font-weight: bold;
                  text-align: left;
                  line-height: 16px;
                  height: 16px;
                  position: relative;
                  color: #000;
                }

                .tooltipster-text {
                  font-size: 12px;
                  text-align: left;
                  line-height: 22px;
                  color: #666;
                }
              </style>
              <script type="text/javascript" src="./raw/qrcode.min.js.download"></script>
              <script type="text/javascript">
                jQuery(function () {

                  var qrcode = new QRCode("qrcode", {
                    text: 'http://www.zaobao.com/znews/singapore/story20191226-1016181',
                    width: 200,
                    height: 200,
                    colorDark: "#000000",
                    colorLight: "#ffffff",
                    correctLevel: QRCode.CorrectLevel.H
                  });

                  jQuery('.btn-wechat').tooltipster({
                    content: 'Loading...',
                    updateAnimation: false,
                    trigger: 'click',
                    contentAsHTML: true,
                    theme: 'tooltipster-shadow',
                    functionBefore: function (origin, continueTooltip) {
                      continueTooltip();

                      var newContent = $('#qrcode img').attr('src');

                      origin.tooltipster('content', '<p class="tooltipster-text-bold">分享到微信朋友圈</p><img src="' + newContent + '"><p class="tooltipster-text">打开微信，点击底部的“发现”，<br/>使用“扫一扫”即可将网页分享至朋友圈。</p>');
                    }
                  });
                });

                function share(type) {

                  switch (type) {

                    case 'facebook':
                      FB.ui({
                        method: 'feed',
                        name: $('meta[property="og:title"]').attr("content"),
                        link: $('meta[property="og:url"]').attr("content"),
                        picture: $('meta[property="og:image"]').attr("content"),
                        caption: '联合早报',
                        description: $('meta[property="og:description"]').attr("content"),
                        message: ''
                      });
                      break;
                  }

                }
              </script>
              <!-- aside-secondary - START-->
              <aside class="aside aside-secondary js-is-sticky js-is-sticky--change" id="js-aside-secondary"
                style="top: 220px; position: sticky;">
                <div class="aside-social ">
                  <div class="a2a_kit a2a_kit_size_32" data-a2a-icon-color="transparent"
                    ng-controller="SharingController" title="" image="" description=""
                    url="http://www.zaobao.com/znews/singapore/story20191226-1016181" caption="联合早报"
                    style="line-height: 32px;">
                    <div class="aside-social-icon"><a class="a2a_button_facebook"
                        onclick="javascript:share(&#39;facebook&#39;);" href="https://www.zaobao.com.sg/#facebook"
                        target="_blank" rel="nofollow noopener"><span class="a2a_svg a2a_s__default a2a_s_facebook"
                          style="background-color: transparent;"><svg focusable="false"
                            xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                            <path fill="#FFF"
                              d="M17.78 27.5V17.008h3.522l.527-4.09h-4.05v-2.61c0-1.182.33-1.99 2.023-1.99h2.166V4.66c-.375-.05-1.66-.16-3.155-.16-3.123 0-5.26 1.905-5.26 5.405v3.016h-3.53v4.09h3.53V27.5h4.223z">
                            </path>
                          </svg></span><span class="a2a_label">Facebook</span></a></div>
                    <div class="aside-social-icon"><a class="a2a_button_twitter analytics_onclick"
                        data-region="article_share::twitter" data-title="" target="_blank"
                        href="https://www.zaobao.com.sg/#twitter" rel="nofollow noopener"><span
                          class="a2a_svg a2a_s__default a2a_s_twitter" style="background-color: transparent;"><svg
                            focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                            <path fill="#FFF"
                              d="M28 8.557a9.913 9.913 0 0 1-2.828.775 4.93 4.93 0 0 0 2.166-2.725 9.738 9.738 0 0 1-3.13 1.194 4.92 4.92 0 0 0-3.593-1.55 4.924 4.924 0 0 0-4.794 6.049c-4.09-.21-7.72-2.17-10.15-5.15a4.942 4.942 0 0 0-.665 2.477c0 1.71.87 3.214 2.19 4.1a4.968 4.968 0 0 1-2.23-.616v.06c0 2.39 1.7 4.38 3.952 4.83-.414.115-.85.174-1.297.174-.318 0-.626-.03-.928-.086a4.935 4.935 0 0 0 4.6 3.42 9.893 9.893 0 0 1-6.114 2.107c-.398 0-.79-.023-1.175-.068a13.953 13.953 0 0 0 7.55 2.213c9.056 0 14.01-7.507 14.01-14.013 0-.213-.005-.426-.015-.637.96-.695 1.795-1.56 2.455-2.55z">
                            </path>
                          </svg></span><span class="a2a_label">Twitter</span></a></div>
                    <div class="aside-social-icon"> <a class="analytics_onclick a2a_button_wechat"
                        data-region="article_share::wechat" data-title="" target="_blank"
                        href="https://www.zaobao.com.sg/#wechat" rel="nofollow noopener"><span
                          class="a2a_svg a2a_s__default a2a_s_wechat" style="background-color: transparent;"><svg
                            focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                            <g fill="#FFF">
                              <path
                                d="M20.674 12.458c-2.228.116-4.165.792-5.738 2.318-1.59 1.542-2.315 3.43-2.116 5.772-.87-.108-1.664-.227-2.462-.294-.276-.023-.602.01-.836.14-.774.438-1.517.932-2.397 1.482.16-.73.266-1.37.45-1.985.137-.45.074-.7-.342-.994-2.673-1.89-3.803-4.714-2.958-7.624.78-2.69 2.697-4.323 5.302-5.173 3.555-1.16 7.55.022 9.712 2.845a6.632 6.632 0 0 1 1.38 3.516zm-10.253-.906c.025-.532-.44-1.01-.984-1.027a.997.997 0 0 0-1.038.964.984.984 0 0 0 .977 1.02 1.017 1.017 0 0 0 1.05-.96zm5.35-1.028c-.55.01-1.01.478-1 1.012.01.554.466.987 1.03.98a.982.982 0 0 0 .99-1.01.992.992 0 0 0-1.02-.982z">
                              </path>
                              <path
                                d="M25.68 26.347c-.705-.314-1.352-.785-2.042-.857-.686-.072-1.408.324-2.126.398-2.187.224-4.147-.386-5.762-1.88-3.073-2.842-2.634-7.2.92-9.53 3.16-2.07 7.795-1.38 10.022 1.493 1.944 2.51 1.716 5.837-.658 7.94-.687.61-.934 1.11-.493 1.917.086.148.095.336.14.523zm-8.03-7.775c.448 0 .818-.35.835-.795a.835.835 0 0 0-.83-.865.845.845 0 0 0-.84.86c.016.442.388.8.834.8zm5.176-1.658a.83.83 0 0 0-.824.794c-.02.47.347.858.813.86.45 0 .807-.34.823-.79a.825.825 0 0 0-.812-.864z">
                              </path>
                            </g>
                          </svg></span><span class="a2a_label">WeChat</span></a></div>
                    <div class="aside-social-icon"><a class="a2a_button_whatsapp analytics_onclick"
                        data-region="article_share::whatsapp" data-title="" target="_blank"
                        href="https://www.zaobao.com.sg/#whatsapp" rel="nofollow noopener"><span
                          class="a2a_svg a2a_s__default a2a_s_whatsapp" style="background-color: transparent;"><svg
                            focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                            <path fill-rule="evenodd" clip-rule="evenodd" fill="#FFF"
                              d="M16.21 4.41C9.973 4.41 4.917 9.465 4.917 15.7c0 2.134.592 4.13 1.62 5.832L4.5 27.59l6.25-2.002a11.241 11.241 0 0 0 5.46 1.404c6.234 0 11.29-5.055 11.29-11.29 0-6.237-5.056-11.292-11.29-11.292zm0 20.69c-1.91 0-3.69-.57-5.173-1.553l-3.61 1.156 1.173-3.49a9.345 9.345 0 0 1-1.79-5.512c0-5.18 4.217-9.4 9.4-9.4 5.183 0 9.397 4.22 9.397 9.4 0 5.188-4.214 9.4-9.398 9.4zm5.293-6.832c-.284-.155-1.673-.906-1.934-1.012-.265-.106-.455-.16-.658.12s-.78.91-.954 1.096c-.176.186-.345.203-.628.048-.282-.154-1.2-.494-2.264-1.517-.83-.795-1.373-1.76-1.53-2.055-.158-.295 0-.445.15-.584.134-.124.3-.326.45-.488.15-.163.203-.28.306-.47.104-.19.06-.36-.005-.506-.066-.147-.59-1.587-.81-2.173-.218-.586-.46-.498-.63-.505-.168-.007-.358-.038-.55-.045-.19-.007-.51.054-.78.332-.277.274-1.05.943-1.1 2.362-.055 1.418.926 2.826 1.064 3.023.137.2 1.874 3.272 4.76 4.537 2.888 1.264 2.9.878 3.43.85.53-.027 1.734-.633 2-1.297.266-.664.287-1.24.22-1.363-.07-.123-.26-.203-.54-.357z">
                            </path>
                          </svg></span><span class="a2a_label">WhatsApp</span></a></div>
                    <div class="aside-social-icon"><a class="a2a_button_email analytics_onclick"
                        data-region="article_share::email" data-title="" target="_blank"
                        href="https://www.zaobao.com.sg/#email" rel="nofollow noopener"><span
                          class="a2a_svg a2a_s__default a2a_s_email" style="background-color: transparent;"><svg
                            focusable="false" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 32 32">
                            <path fill="#FFF"
                              d="M26 21.25v-9s-9.1 6.35-9.984 6.68C15.144 18.616 6 12.25 6 12.25v9c0 1.25.266 1.5 1.5 1.5h17c1.266 0 1.5-.22 1.5-1.5zm-.015-10.765c0-.91-.265-1.235-1.485-1.235h-17c-1.255 0-1.5.39-1.5 1.3l.015.14s9.035 6.22 10 6.56c1.02-.395 9.985-6.7 9.985-6.7l-.015-.065z">
                            </path>
                          </svg></span><span class="a2a_label">Email</span></a></div>
                  </div>
                </div>

                <script type="text/javascript" src="./raw/page.js.download"></script>
              </aside>

            </div>
            <div class="col-12 col-lg-11 col-xxl-11 content-container" id="Milk">
              <!-- content-primary - START-->
              <div class="content content-primary" id="js-content-primary">

                <div class="breadcrumbs">
                  <ul class="breadcrumbs-list">
                    <i class="fa fa-angle-right"></i>
                    <li><a href="https://www.zaobao.com.sg/znews">新闻</a></li> <i class="fa fa-angle-right"></i>
                    <li><a href="https://www.zaobao.com.sg/znews/singapore">新加坡</a></li>
                  </ul>
                </div>

                <div id="block-views-2225f755c990b57decaccd5cbe0c198b" class="block block-views">
                  <div class="content">
                    <div
                      class="view view-zb2016-node-content view-id-zb2016_node_content view-display-id-zb2019_node_content view-dom-id-90f09ef6fefce05e9887f09303164c3b">

                      <div class="view-content">
                        <div class="views-row views-row-1 views-row-odd views-row-first views-row-last">
                          <div about="/znews/singapore/story20191226-1016181" typeof="sioc:Item foaf:Document"
                            class="ds-1col node node-article view-mode-2019_content  clearfix">

                            <div class="article-title">
                              <h1>ZAOBAO TEMPLATE</h1>
                            </div>
                            <div class="article-meta">
                              <div class="row no-gutters align-items-end">
                                <div class="col-12 col-xl article-byline">
                                  <h4 class="title-byline byline">文 / <a
                                      href="https://www.zaobao.com.sg/byline/XXX">XXX</a></h4>
                                  <h4 class="title-byline frontend-developer">XXX / <a
                                      href="https://www.zaobao.com.sg/frontend-developer/XXX">XXX</a></h4>
                                  <h4 class="title-byline interactive-designer">交互设计 / <a
                                      href="https://www.zaobao.com.sg/interactive-designer/XXX">XXX</a></h4>
                                  <h4 class="title-byline photographer">摄影 / <a
                                      href="https://www.zaobao.com.sg/photographer/XXX">​​​​​​​XXX</a></h4>
                                  
                                </div>
                              </div>
                            </div>
                            <div class="inline-figure">
                              <!--0 video, 1 or multiple image -->
                              <div style="display:none;"></div>
                            </div>
                            <!-- <figure class="inline-figure inline-figure-img">
                              <div class="figure-media"><img class="img-fluid"
                                  src="./raw/20191226_news_eclipse.jpeg"
                                  title="今早接近11时30分，月亮开始移到太阳与地球之间，形成日环食现象，这是时隔21年再次出现的情况。（何家俊摄）" alt=""></div>
                              <figcaption>今早接近11时30分，月亮开始移到太阳与地球之间，形成日环食现象，这是时隔21年再次出现的情况。（何家俊摄）</figcaption>
                            </figure> -->
                            <div class="field field-name-2019-font-size">
                              <!-- article-fontsizing - START-->
                              <div class="article-fontsizing">
                                <div class="fontsizing-title">
                                  <p>字体大小:</p>
                                </div>
                                <ul class="fontsizing-range-list js-fontsizing">
                                  <li class="is-active" data-fontsize="10">小</li>
                                  <li data-fontsize="11">中</li>
                                  <li data-fontsize="12">大</li>
                                </ul>
                              </div>
                              <!-- article-fontsizing - END-->
                            </div>
                            <div class="article-content js-fontsized" style="font-size: 10px;">