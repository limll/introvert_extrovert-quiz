<?php include 'assets/inc/header.php'?>
<?php include 'assets/inc/body.php'?>
    <div class="article-content-rawhtml">
    <link href="//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/css/quiz_style.css" rel="stylesheet" />
<link href="//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/css/style.css" rel="stylesheet" />
        <div class="container">
        <div class="row justify-content-center">
                <div class="col-md-8 col-12">
                <h2>内向或外向 都难逃疫情的影响</h2>
                <p>人们在防疫期间尽量不出门，时间长了或让一些人再与亲友社交时，感到焦虑。
你是热衷于交新朋友，会在多人的聚会中得到一份快乐的外向者？还是你是安静低调，喜欢在独自进行活动中获得一份慰藉的内向者？
</p>
<p>但冠病疫情不在乎我们究竟内向或外向——无论什么性格，人们都必须在生活上做出调整。
</p>
<p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">i hate when i’m nice to someone once and they get too comfortable like okay now i have to awkwardly tell you to back up before i short circuit that sucks</p>&mdash; your power (@gaialect) <a href="https://twitter.com/gaialect/status/1287367218255351809?ref_src=twsrc%5Etfw">July 26, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></p>
<p>社交机会在疫情期减少了，内向者真的比外向者快乐吗？自认内向的网民在居家防疫的第20天就在推特上分享自己已开始想念和朋友见面和聊天。
</p>
<p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">okay as an introvert i really love staying home but with the circuit breaker and everything, staying home feels different now. Im being forced to stay home.</p>&mdash; Nyx Senju (@DcruzDanial) <a href="https://twitter.com/DcruzDanial/status/1254297638792355848?ref_src=twsrc%5Etfw">April 26, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></p>
<p>也有网民指出，内向者平时确实喜欢待在家中，但疫情下是被迫待在家里，为了防疫必须居家与自己选择不出门还是不同的。
</p>
<p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">Someone help me make sense of this. I’m such an extrovert but I have the worst social anxiety</p>&mdash; Ali Johnson (@Ali__Lee) <a href="https://twitter.com/Ali__Lee/status/1284512962313625600?ref_src=twsrc%5Etfw">July 18, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></p>
<p>新加坡解封后，有自称外向的网民也在推特上求救，不解自己为何在社交上感到焦虑。外向者一向被认为喜欢与人社交。但外向者居家多日后，再与亲友见见面时，未必无忧无虑。
</p>
<p><blockquote class="twitter-tweet"><p lang="en" dir="ltr">my introvert self feels stressed when people start to jio me out during phase 2 I’m..... not prepared to meet anyone.......</p>&mdash; ¹ᵇjonginnie (@cutehom) <a href="https://twitter.com/cutehom/status/1276045359161028608?ref_src=twsrc%5Etfw">June 25, 2020</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script></p>
<p>也有内向的网民在推文上坦言自己“还没有准备好见任何人，朋友在解封第二阶段开始约我出来，让我感到压力。”
</p>
</div>
       
            </div>
        </div>
        <div class="container" id="quiz" ng-app="App.controllers" ng-controller="MainCtrl" ng-cloak>
        
            <div class="row justify-content-center my-xl-5">

            <div class="col-md-10 col-12 start-page mb-5" ng-if="!quiz_started">
            <div class="quiz-intro"><img src="//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/quiz-intro-title.svg"></div>
            <div class="quiz-intro-time"><img src="//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/imgs/quiz-intro-time.svg">
            <button class="btn start-btn" ng-click="start()">开始答题</button>
            </div>
                
            </div>

            <div class="col-md-10 quiz mb-5" ng-if="quiz_started && !quiz_ended">
           
        
                <div class="img">
                <img ng-src="{{qns_img}}" class="view-desktop">
                <img ng-src="{{qns_mobile_img}}" class="view-mobile">
                <div class="progress-circle">
                <div class="outer">
                    <div class="half spinner"></div>
                    <div class="half filler"></div>
                    <div class="mask"></div>
                </div>
                    <div class="inner"></div>
                    <div class="percentage">0%</div>
                   
                </div>

                <div class="qnsWrap">
                    <div class="qns">
                        <div class="qnsNo">{{id + 1}}.</div> {{qns}}
                    </div>
                    <div class="choices">
                        <button class="quiz-button" ng-repeat="c in choices" ng-click="nextQns(c.id);" ng-disabled="showAns">
                            <!-- <em class="choice">{{c.id}}</em> -->
                            <span class="jumbo">
                                {{c.choice}}
                                <!-- <img ng-src="c.img" ng-if="c.img !=''"> -->
                            </span>
                        </button>

                        <!-- <button class="btn prev-qns quiz-btn" ng-if="qns_num <= 5 && qns_num != 1 && !showAns" ng-click="prevQns();getQuestion()">上一题</button> -->
                    </div>

                  
                </div>
                </div>

            </div>
            
          
          
        
            <div class="col-md-10 end-page text-center" ng-if="quiz_ended">
            <div class="result_pic view-desktop" ng-bind-html="result_img">{{result_img}}</div>
            <div class="result_pic view-mobile" ng-bind-html="result_img_mobile">{{result_img_mobile}}</div>
                <!-- <p class="result"><span class="title">得分</span><br /> -->
             
               <!-- <div class="score">
                   {{mood}}<br/>
                   {{score}}<br/>
                   {{percentage}}<em>%</em>
                </div> -->
               <div class="mood-meter">
                   <!-- 84 to 100 -->
                   <div class="mood introvert-10" ng-class="{active:(mood == 'introvert' && percentage >= 84 && percentage <= 100)}">{{percentage}}%</div>
                   <!-- 63 to 83 -->
                   <div class="mood introvert-20" ng-class="{active:(mood == 'introvert' && percentage >= 63 && percentage <= 83)}">{{percentage}}%</div>
                   <!-- 42 to 62 -->
                   <div class="mood introvert-30" ng-class="{active:(mood == 'introvert' && percentage >= 42 && percentage <= 62)}">{{percentage}}%</div>
                   <!-- 21 to 41 -->
                   <div class="mood introvert-40" ng-class="{active:(mood == 'introvert' && percentage >= 21 && percentage <= 41)}">{{percentage}}%</div>
                   <!-- 0 to 20 -->
                   <div class="mood introvert-50" ng-class="{active:(mood == 'introvert' && percentage >= 0 && percentage <= 20)}">{{percentage}}%</div>
                   <div class="mood ambivert-50" ng-class="{active:(mood == 'ambivert')}">50%</div>
                   <!-- 0 to 20 -->
                   <div class="mood extrovert-10" ng-class="{active:(mood == 'extrovert' && percentage >= 0 && percentage <= 20)}">{{percentage}}%</div>
                   <!-- 21 to 41 -->
                   <div class="mood extrovert-20" ng-class="{active:(mood == 'extrovert' && percentage >= 21 && percentage <= 41)}">{{percentage}}%</div>
                   <!-- 42 to 62 -->
                   <div class="mood extrovert-30" ng-class="{active:(mood == 'extrovert' && percentage >= 42 && percentage <= 62)}">{{percentage}}%</div>
                   <!-- 63 to 83 -->
                   <div class="mood extrovert-40" ng-class="{active:(mood == 'extrovert' && percentage >= 63 && percentage <= 83)}">{{percentage}}%</div>
                   <!-- 84 to 100 -->
                   <div class="mood extrovert-50"  ng-class="{active:(mood == 'extrovert' && percentage >= 84 && percentage <= 100)}">{{percentage}}%</div>

                   <div class="bg-wrapper"> 
                   <div class="mm-text">内向</div>
                   <div class="bg introvert-10-" ng-class="{active:(mood == 'introvert' && percentage >= 84 && percentage <= 100)}"></div>
                   <div class="bg introvert-20-" ng-class="{active:(mood == 'introvert' && percentage >= 63 && percentage <= 83)}"></div>
                   <div class="bg introvert-30" ng-class="{active:(mood == 'introvert' && percentage >= 42 && percentage <= 62)}"></div>
                   <div class="bg introvert-40"  ng-class="{active:(mood == 'introvert' && percentage >= 21 && percentage <= 41)}"></div>
                   <div class="bg introvert-50" ng-class="{active:(mood == 'introvert' && percentage >= 0 && percentage <= 20)}"></div>
                   <div class="bg ambivert-50" ng-class="{active:(mood == 'ambivert')}"></div>
                   <div class="bg extrovert-10" ng-class="{active:(mood == 'extrovert' && percentage >= 0 && percentage <= 20)}"></div>
                   <div class="bg extrovert-20" ng-class="{active:(mood == 'extrovert' && percentage >= 21 && percentage <= 41)}"></div>
                   <div class="bg extrovert-30" ng-class="{active:(mood == 'extrovert' && percentage >= 42 && percentage <= 62)}"></div>
                   <div class="bg extrovert-40" ng-class="{active:(mood == 'extrovert' && percentage >= 63 && percentage <= 83)}"></div>
                   <div class="bg extrovert-50"  ng-class="{active:(mood == 'extrovert' && percentage >= 84 && percentage <= 100)}"></div>
                   <div class="mm-text">外向</div>
                </div>
               

                    <div class="ambivert-txt">中间性格</div>
                    <div class="buttons">
                        <a class="btn share" ng-href="//www.facebook.com/sharer/sharer.php?u={{shareFB_url}}"
                                                target="_blank">分享</a>
                        <button class="btn restart" ng-click="restart()">再度挑战</button>
                    </div>
                </div>
               <div class="personality" ng-class="{introvert : score < 0, extrovert: score > 0, ambivert: score == 0}" ><h2 class="status-hd">{{status}}</h2>
               <p class="result_desc" ng-bind-html="result_desc">{{result_desc}}</p>

                <div class="graph">
                    
                    
                 
                </div>
               </div>
              
                <!-- <div class="score"><span>{{score}}</span>/10</div> -->
                
               
                

               





            </div>
        </div>

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8 col-12">
                <h2>居家防疫期间 失去自由的挣扎</h2>
                <p>我们以偏概全地认为，内向者在居家期间不用社交一定乐坏了，是否将事情想得太过简单？
<a href="https://www.mindwhatmatters.com.sg/" target="_blank">Mind what Matters</a>诊所心理学家杨伟文博认为，人们习惯性地认为内向的人不须要社交，但人天生就是社交动物，每个人都有想要和别人互动的倾向。“内向者少了社交未必好过。”
他也指出，“自我决定论”（Self-determination Theory，简称SDT）提到，“自主”是人类与生俱来的心理需求之一。
</p>
<img src="assets/imgs/lifestyle_personality1.jpg" class="img-fluid">
<p>人类天生对自由有所向往，因此防疫期间被“束缚”在家中的我们或感到焦虑不安。
“自主”指一个人是否有权利随心所欲地发表内心的想法，又能否自我决策自己的行为。如果“自主”的心理需求无法被满足，将打击一个人身心健康。
</p>
<p>杨伟文博士说：“对自由有向往是人类的天性，无关性格。疫情期间，法律规定我们为了防疫尽量不出门。几个月下来，我们或感觉被约束，逐渐感到焦虑且不安。”
</p>
<p><a href="https://sofia.com.sg/" target="_blank">Sofia Wellness</a>诊所心理治疗师吴诗韵也说，任何性格的人在疫情下面对未知都可能感到情绪低落。“有些人或许需要按照某种规律生活，才能感觉自己拥有掌控生活的主权。当生活作息规律被打乱，这些人或感觉生活失去了方向，挣扎于生活。”
</p>
<h2>解封后 再见面时的社交焦虑</h2>
<img src="assets/imgs/lifestyle_personality2.jpg" class="img-fluid">
<p>当人们不清楚什么才是社交的新常态，与亲友见面时或感到焦虑。
内向者在居家防疫期间不一定好过，那外向者在解封后，重新与亲友见面时感到焦虑是否正常？
</p>
<p>杨伟文博士说：“人们疫情期间减少与亲友面对面的交流，社交习惯也改变了。当社会重新开启，人们需要时间适应与别人交流的感觉。过程中或因为不习惯而感到焦虑。外向者也可能感到焦虑。”
</p>
<p>吴诗韵也说：“冠病疫情发生前，拥抱和握手是人们熟悉的社交方式。但人们在疫情下避免与别人有肢体上的接触。我们不清楚什么才是社交的新常态，遇见亲友时不知该如何反应，因此对社交感到不安和焦虑。”
</p>
<p>杨伟文博士提到，有些人或认为解封后就“必须”马上出去社交。“但未先做好心理准备，就急于一时地逼着自己社交，很可能会加剧社交焦虑。</p>
<p>他说：“或许我们也可以趁着疫情期间去思考什么对我们来说才是重要的。有些友谊如果影响我们的身心健康，我们是否还想继续保留这些友谊？”
</p>
<h2>除了内向和外向 还能分中间性格</h2>
<img src="assets/imgs/lifestyle_personality3.jpg" class="img-fluid">
<p>“外向”与“内向”最简单的定义，是一个人的充电的方式</p>
<p>人们习惯性地给别人贴上“内向”和“外向”的标签，甚至认为同一个性格的人会以相同的方式应对疫情所带来的的困扰。但一个人的性格单以“外向”与“内向”来形容是否足够？
</p>
<p>吴诗韵说，“外向”与“内向”最简单的定义，是一个人的充电方式。“外向者在人群中会感到精力充沛，独处时则觉得无聊，身心疲惫。相反的，内向者重视个人空间，须要时间与世隔绝，沉浸在自己的世界里好好充电。”
</p>
<p>但吴诗韵指出，人的性格不是黑白分明，而是一个连续体。“人们一般介于两个极端的中间。也有人称自己为‘中间性格’（ambivert），意指自己拥有平等的外向和内向特征。”
</p>
<h2>性格是复杂概念 内向或外向不全面</h2>
<img src="assets/imgs/lifestyle_personality4.jpg" class="img-fluid">
<p>性格是个复杂且多层面的概念，“内向”和“外向”难以全面地描述一个人的性格特征。</p>
<p>吴诗韵也说，基因在一个人的性格塑造上扮演着至关重要的角色，“性格往往在我们出世以前已成定局。我们可以在成长的过程中学习新技能，比如说一个内向的人可以学习社交技能。但内向者天生对独处的倾向却是无法改变的。”
</p>
<p>杨伟文博士指出，人们的性格会随着成长中所积累的经验，以及对其解读而出现变化。
他举例说：“当老师大声说话时，学生A可能认为老师心情不好，没有放在心上。但同样的情况，学生B可能以为是自己做错了事情，被老师有意针对。如果学生B陷入自卑的状态，性格塑造上会随之受到负面的影响。”
</p>
<p>杨伟文博士也说：“一个人在不同的场合也可能表现出不一样的性格。例如，当一个人感到不安全时，无论性格内向或外向，都不愿多说话。这是一种本能的自我保护反应。”
他认为，性格是个复杂且多层面的概念，单单用“内向”和“外向”无法全面地描述一个人的性格特征。
</p>
<h2>用4个步骤委婉地说“不”</h2>
<img src="assets/imgs/lifestyle_personality5.jpg" class="img-fluid">
<p>聆听自己心中所想非常重要，必要时要记得说“不”。</p>
<p>“外向”和“内向”终究只是个标签，无法完整地代表一个人。生活中，有些人可能要求我们活在框架中，但越是如此，越应该忠实于自己心里所想，打破束缚。</p>
<p>面对朋友的邀约，有时想要回绝确实不容易。吴诗韵分享4个步骤，帮我们在不想出门的时候，委婉地回绝朋友的邀请：</p>
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">拒绝4步骤</th>
      <th scope="col">拿拒绝朋友约吃火锅为例</th>
    </tr>
  </thead>
  <tbody>
  <tr>
      <td>重复别人的邀请。</td>
      <td>我知道你想吃火锅。</td>
    </tr>
    <tr>
      <td>解释自己不想去的立场。</td>
      <td>但是冠病疫情未过去，我认为现在吃火锅不是很卫生。</td>
    </tr>
    <tr>
      <td>明确地说不。</td>
      <td>我们还是不要在这个时候吃火锅。</td>
    </tr>
    <tr>
      <td>提供其他选择，或是另一个日期。</td>
      <td>不如等疫情过去后，再一起吃火锅？</td>
    </tr>
    </tbody>
</table>
<p>如果某天感到疲惫，不想赴约时，大可勇敢地说“不”。真正关心你，在乎你的人，会尊重你的决定。等你准备好了，还是会发简讯问妳：“想一起吃饭吗？”
</p>
</div>

            </div>
        </div>
</div>

    <script src="//interactive.zaobao.com/lib/js/angular/v1.4.9/angular.min.js"></script>
    <script src='//interactive.zaobao.com/lib/js/angular/angular-sanitize/v1.5.8/angular-sanitize.min.js'></script>
    <script src="//interactive.zaobao.com/2020/introvert-extrovert-quiz/assets/js/quiz.js?05082020h"></script>
   
    </div>
                           
<?php include 'assets/inc/footer.php'?>
</html>